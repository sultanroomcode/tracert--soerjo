<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Tentang';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Aplikasi Tracert Study versi website untuk mahasiswa Alumni Universitas Soerjo Ngawi</p>
</div>
