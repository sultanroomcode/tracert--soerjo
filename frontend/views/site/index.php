<?php
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'Aplikasi Tracert Study Universitas Soerjo Ngawi';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Selamat Datang!</h1>

        <img src="<?=Url::base().'/img/logo.png'?>" class="img-responsive" width="200" style="margin: 50px auto;">

        <p class="lead">selamat datang di website aplikasi tracert study Universitas Soerjo Ngawi.</p>

        <p><a class="btn btn-lg btn-success" href="<?=Url::to(['/site/login'])?>">Login</a></p>
    </div>
</div>
