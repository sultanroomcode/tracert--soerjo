<?php
namespace frontend\modules\profile\models;
// use frontend\modules\profile\models\UserProfile;

use Yii;
use yii\db\ActiveRecord;
use common\models\User;
use frontend\modules\survey\models\SurveyTracert;

/**
 * This is the model class for table "user_profile".
 *
 * @property string $nimhsmsmh NIM
 * @property string $kdptimsmh Kode PT
 * @property string $kdpstmsmh Kode Prodi
 * @property string $nmmhsmsmh Nama Mahasiswa
 * @property string $tahun_lulus
 * @property string $telpomsmh
 * @property string $emailmsmh
 * @property int $created_at
 * @property int $updated_at
 */
class UserProfile extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_profile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nimhsmsmh', 'kdptimsmh', 'kdpstmsmh', 'nmmhsmsmh', 'tahun_lulus', 'telpomsmh', 'emailmsmh'], 'required'],
            [['nimhsmsmh', 'kdptimsmh', 'kdpstmsmh'], 'string', 'max' => 15],
            [['nmmhsmsmh'], 'string', 'max' => 255],
            [['gender'], 'string', 'max' => 1],
            [['tahun_lulus'], 'string', 'max' => 4],
            [['telpomsmh'], 'string', 'max' => 40],
            [['emailmsmh'], 'string', 'max' => 150],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nimhsmsmh' => 'NPM',
            'kdptimsmh' => 'Kode PT',
            'kdpstmsmh' => 'kode Prodi',
            'nmmhsmsmh' => 'Nama Mahasiswa',
            'gender' => 'Jenis Kelamin',
            'tahun_lulus' => 'Tahun Lulus',
            'telpomsmh' => 'Telpon Mahasiswa',
            'emailmsmh' => 'Email Mahasiswa',
            'created_at' => 'Tanggal Dibuat',
            'updated_at' => 'Tanggal Diupdate',
        ];
    }

    public function getArrProdi()
    {
        return [
            '54211' => 'Agroteknologi',
            '63201' => 'Ilmu Administrasi Negara',
            '74201' => 'Ilmu Hukum',
            '61201' => 'Manajemen',
            '21201' => 'Teknik Mesin',
            '22201' => 'Teknik Sipil',
        ];
    }

    public function getUserone()
    {
        return $this->hasOne(User::className(), ['username' => 'nimhsmsmh']);
    }

    public function getSurvey()
    {
        return $this->hasOne(SurveyTracert::className(), ['nimhsmsmh' => 'nimhsmsmh']);
    }
}
