<?php
namespace frontend\modules\profile\controllers;

use Yii;
use common\models\User;
use frontend\modules\profile\models\UserProfile;
use frontend\models\UserSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\Query;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
        	'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'cariModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post())){
            $model->email = $model->username.'@baru.com';
            $model->expired_at = time();
            $model->setPassword($model->password_hash);
            $model->generateAuthKey();
            $model->generateAccessToken();
            $model->generateEmailVerificationToken();

            if($model->save()) {
                $modprof = new UserProfile();
                $modprof->nimhsmsmh = $model->username;
                $modprof->kdptimsmh = '071045';
                $modprof->kdpstmsmh = '63201';
                $modprof->nmmhsmsmh = $model->username;
                $modprof->gender = 'L';
                $modprof->tahun_lulus = date('Y');
                $modprof->telpomsmh = '000000000';
                $modprof->emailmsmh = $model->username.'@baru.com';
                $modprof->created_at = $modprof->updated_at = time();

                if($modprof->save()){
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())){
            $model->setPassword($model->password_hash);
            
            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
    	$connection = Yii::$app->db;
        $transaction = $connection->beginTransaction();
    	try {
	    	$model = $this->findModel($id);
	    	//remove profile
	    	$model->profileone->delete();
	    	//remove survey
	    	$model->surveyone->delete();
	    	//delete user
	        $model->delete();   
	        $transaction->commit();
	    } catch(Exception $e) {
	        $transaction->rollback();
	    }

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
