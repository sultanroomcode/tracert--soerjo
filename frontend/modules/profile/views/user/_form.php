<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput() ?>

    <?= $form->field($model, 'password_hash')->textInput() ?>
    
    <?= $form->field($model, 'status')->dropdownList([
    	0 => 'Tidak Aktif', 
    	9 => 'Menunggu Konfirmasi Email', 
    	10 => 'Aktif'
    ]) ?>

    <?= $form->field($model, 'type')->dropdownList([
    	'mahasiswa' => 'Mahasiswa',
    	'admin' => 'Admin', 
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
