<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\profile\models\UserProfile */

$this->title = $model->nmmhsmsmh;
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-profile-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->nimhsmsmh], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->nimhsmsmh], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nimhsmsmh',
            'kdptimsmh',
            'kdpstmsmh',
            'nmmhsmsmh',
            'tahun_lulus',
            'telpomsmh',
            'gender',
            'emailmsmh:email',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
