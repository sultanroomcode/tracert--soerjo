<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\profile\models\UserProfile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-profile-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nimhsmsmh')->textInput(['maxlength' => true, 'readonly' => true]) ?>

    <?= $form->field($model, 'kdptimsmh')->textInput(['maxlength' => true, 'readonly' => true]) ?>

    <?= $form->field($model, 'kdpstmsmh')->dropdownList($model->getArrProdi()) ?>

    <?= $form->field($model, 'nmmhsmsmh')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'gender')->dropdownList(['L' => 'Laki-Laki', 'P' => 'Perempuan']) ?>

    <?= $form->field($model, 'tahun_lulus')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telpomsmh')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'emailmsmh')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
