<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\profile\models\UserProfile */

$this->title = 'Update User Profile: ' . $model->nimhsmsmh;
$this->params['breadcrumbs'][] = ['label' => 'User Profiles'];
$this->params['breadcrumbs'][] = ['label' => $model->nimhsmsmh, 'url' => ['view', 'id' => $model->nimhsmsmh]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-profile-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
