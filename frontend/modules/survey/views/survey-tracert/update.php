<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\survey\models\SurveyTracert */

$this->title = 'Update Survey Tracert Study';
$this->params['breadcrumbs'][] = ['label' => $model->nimhsmsmh, 'url' => ['view', 'id' => $model->nimhsmsmh]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="survey-tracert-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>