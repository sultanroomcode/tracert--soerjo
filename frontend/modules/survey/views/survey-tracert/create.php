<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\survey\models\SurveyTracert */

$this->title = 'Isi Survey Tracert Study';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="survey-tracert-create">
    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
