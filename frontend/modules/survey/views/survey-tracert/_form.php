<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\modules\survey\models\SurveyTracert */
/* @var $form yii\widgets\ActiveForm */
if(Yii::$app->user->identity->type != 'admin'){
    $model->nimhsmsmh = Yii::$app->user->identity->username;
}
if($model->isNewRecord){
    $model->f_download = 0;
    $model->f501 = $model->f8 = 1;
    //karena f8 == 1 maka hide1
    $model->f1301 = $model->f1302 = $model->f1303 = 0;
    $first_objHide_f5x = '.field-surveytracert-f503';
    $first_objHide_f12x = '.field-surveytracert-f1202';
} else {
    if($model->f501 == 1)
    {
        $first_objHide_f5x = '.field-surveytracert-f503';
    } else {
        $first_objHide_f5x = '.field-surveytracert-f502';
    }
}

$val_opt = [
    1 => '1',
    2 => '2',
    3 => '3',
    4 => '4',
    5 => '5'
];
$template_radio = [
    'item' => function($index, $label, $name, $checked, $value) {

        $return = '<td>';
        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" tabindex="3">';
        $return .= '</td>';
        // $return .= '<span>' . ucwords($label) . '</span>';
        // $return .= '</label>';

        return $return;
    }
];
?>
<style>
    .a_side {
        background: #FF0;
    }

    .b_side {
        background: #F00;
    }
</style>
<div class="survey-tracert-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if(Yii::$app->user->identity->type != 'admin'){ ?>
        <?= $form->field($model, 'nimhsmsmh')->hiddenInput(['maxlength' => true])->label(false) ?>
    <?php } else { ?>
        <?= $form->field($model, 'nimhsmsmh')->textInput(['maxlength' => true]) ?>
    <?php } ?>

    <!-- f5 -->
    <div class="row" id="f5-form-zone">
        <div class="col-md-1"><b>F5</b></div>
        <div class="col-md-5">
            Berapa bulan waktu yang dihabiskan (sebelum dan sesudah kelulusan) untuk memeroleh pekerjaan pertama? 
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'f502')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'f503')->textInput(['maxlength' => true]) ?> Bulan
            <?= $form->field($model, 'f501')->radioList(['1' => 'Sebelum Lulus', '2' => 'Sesudah Lulus'])->label(false) ?>
        </div>
    </div>

    <hr>

    <!-- f12 -->
    <div class="row" id="f12-form-zone">
        <div class="col-md-1"><b>F12</b></div>
        <div class="col-md-5">
            Sebutkan sumberdana dalam pembiayaan kuliah? 
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'f1201')->inline(false)->radioList(
            ['1' => 'Biaya Sendiri / Keluarga',
            '2' => 'Beasiswa ADIK',
            '3' => 'Beasiswa BIDIKMISI',
            '4' => 'Beasiswa PPA',
            '5' => 'Beasiswa AFIRMASI',
            '6' => 'Beasiswa Perusahaan/Swasta',
            '7' => 'Lainnya, tuliskan: ']
            )->label(false) ?>
            <?= $form->field($model, 'f1202')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <hr>

    <!-- f8 -->
    <div class="row" id="f8-form-zone">
        <div class="col-md-1"><b>F8</b></div>
        <div class="col-md-5">
            Apakah anda bekerja saat ini (termasuk kerja sambilan dan wirausaha)? 
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'f8')->inline(false)->radioList(
            ['1' => '[1] Ya',
            '2' => '[2] Tidak']
            )->label(false) ?>
        </div>
    </div>

    <hr>

    <!-- f14 -->
    <div class="row" id="f14-form-zone">
        <div class="col-md-1"><b>F14</b></div>
        <div class="col-md-5">
             Seberapa erat hubungan antara bidang studi dengan pekerjaan anda? 
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'f14')->inline(false)->radioList(
            ['1' => '[1] Sangat Erat',
            '2' => '[2] Erat',
            '3' => '[3] Cukup Erat',
            '4' => '[4] Kurang Erat',
            '5' => '[5] Tidak Sama Sekali']
            )->label(false) ?>
        </div>
    </div>
    
    <hr>

    <!-- f15 -->
    <div class="row" id="f15-form-zone">
        <div class="col-md-1"><b>F15</b></div>
        <div class="col-md-5">
            Tingkat pendidikan apa yang paling tepat/sesuai untuk pekerjaan anda saat ini? (F15)
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'f15')->inline(false)->radioList(
            ['1' => '[1] Setingkat Lebih Tinggi',
            '2' => '[2] Tingkat yang Sama',
            '3' => '[3] Setingkat Lebih Rendah',
            '4' => '[4] Tidak Perlu Pendidikan Tinggi',]
            )->label(false) ?>
        </div>
    </div>

    <hr>

    <!-- f13 -->
    <div class="row" id="f13-form-zone">
        <div class="col-md-1"><b>F13</b></div>
        <div class="col-md-5">
            Kira-kira berapa pendapatan anda setiap bulannya? 
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'f1301')->textInput(['maxlength' => true, 'placeholder' => '(Isilah dengan ANGKA saja, tanpa tanda Titik atau Koma)']) ?>

            <?= $form->field($model, 'f1302')->textInput(['maxlength' => true, 'placeholder' => '(Isilah dengan ANGKA saja, tanpa tanda Titik atau Koma)']) ?>

            <?= $form->field($model, 'f1303')->textInput(['maxlength' => true, 'placeholder' => '(Isilah dengan ANGKA saja, tanpa tanda Titik atau Koma)']) ?>
        </div>
    </div>

    <!-- f2 -->
    <div class="row" id="f2-form-zone">
        <div class="col-md-1"><b>F2</b></div>
        <div class="col-md-5">
            Menurut anda seberapa besar penekanan pada metode pembelajaran di bawah ini dilaksanakan di program studi anda?
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'f21')->inline(false)->radioList(
            ['1' => '[1] Sangat Besar',
            '2' => '[2] Besar',
            '3' => '[3] Cukup Besar',
            '4' => '[4] Kurang Besar',
            '5' => '[5] Tidak Sama Sekali']
            ) ?>
            <hr>
            <?= $form->field($model, 'f22')->inline(false)->radioList(
            ['1' => '[1] Sangat Besar',
            '2' => '[2] Besar',
            '3' => '[3] Cukup Besar',
            '4' => '[4] Kurang Besar',
            '5' => '[5] Tidak Sama Sekali']
            ) ?>
            <hr>
            <?= $form->field($model, 'f23')->inline(false)->radioList(
            ['1' => '[1] Sangat Besar',
            '2' => '[2] Besar',
            '3' => '[3] Cukup Besar',
            '4' => '[4] Kurang Besar',
            '5' => '[5] Tidak Sama Sekali']
            ) ?>
            <hr>
            <?= $form->field($model, 'f24')->inline(false)->radioList(
            ['1' => '[1] Sangat Besar',
            '2' => '[2] Besar',
            '3' => '[3] Cukup Besar',
            '4' => '[4] Kurang Besar',
            '5' => '[5] Tidak Sama Sekali']
            ) ?>
            <hr>
            <?= $form->field($model, 'f25')->inline(false)->radioList(
            ['1' => '[1] Sangat Besar',
            '2' => '[2] Besar',
            '3' => '[3] Cukup Besar',
            '4' => '[4] Kurang Besar',
            '5' => '[5] Tidak Sama Sekali']
            ) ?>
            <hr>
            <?= $form->field($model, 'f26')->inline(false)->radioList(
            ['1' => '[1] Sangat Besar',
            '2' => '[2] Besar',
            '3' => '[3] Cukup Besar',
            '4' => '[4] Kurang Besar',
            '5' => '[5] Tidak Sama Sekali']
            ) ?>
            <hr>
            <?= $form->field($model, 'f27')->inline(false)->radioList(
            ['1' => '[1] Sangat Besar',
            '2' => '[2] Besar',
            '3' => '[3] Cukup Besar',
            '4' => '[4] Kurang Besar',
            '5' => '[5] Tidak Sama Sekali']
            ) ?>
        </div>
    </div>

    <!-- f3 -->
    <div class="row" id="f3-form-zone">
        <div class="col-md-1"><b>F3</b></div>
        <div class="col-md-5">
            Kapan anda mulai mencari pekerjaan? Mohon pekerjaan sambilan tidak dimasukkan
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'f301')->inline(false)->radioList(
            ['1' => '[1] Kira-kira ... bulan sebelum lulus',
            '2' => '[2] Kira-kira ... bulan sesudah lulus',
            '3' => '[3] Saya tidak mencari kerja (Langsung ke pertanyaan f8)'
            ]) ?>
            <?= $form->field($model, 'f302')->textInput(['maxlength' => true, 'placeholder' => '(Isilah dengan ANGKA saja)']) ?>

            <?= $form->field($model, 'f303')->textInput(['maxlength' => true, 'placeholder' => '(Isilah dengan ANGKA saja)']) ?>
        </div>
    </div>

    <!-- f4 -->
    <div class="row" id="f4-form-zone">
        <div class="col-md-1"><b>F4</b></div>
        <div class="col-md-5">
            Bagaimana anda mencari pekerjaan tersebut? Jawaban bisa lebih dari satu
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'f401')->checkbox() ?>

            <?= $form->field($model, 'f402')->checkbox() ?>

            <?= $form->field($model, 'f403')->checkbox() ?>

            <?= $form->field($model, 'f404')->checkbox() ?>

            <?= $form->field($model, 'f405')->checkbox() ?>

            <?= $form->field($model, 'f406')->checkbox() ?>

            <?= $form->field($model, 'f407')->checkbox() ?>

            <?= $form->field($model, 'f408')->checkbox() ?>

            <?= $form->field($model, 'f409')->checkbox() ?>

            <?= $form->field($model, 'f410')->checkbox() ?>

            <?= $form->field($model, 'f411')->checkbox() ?>

            <?= $form->field($model, 'f412')->checkbox() ?>

            <?= $form->field($model, 'f413')->checkbox() ?>

            <?= $form->field($model, 'f414')->checkbox() ?>

            <?= $form->field($model, 'f415')->checkbox() ?>
            
            <?= $form->field($model, 'f416')->textInput() ?>
        </div>
    </div>

    <!-- f6 -->
    <div class="row" id="f6-form-zone">
        <div class="col-md-1"><b>F6</b></div>
        <div class="col-md-5">
            Berapa perusahaan/instansi/institusi yang sudah anda lamar (lewat surat atau e-mail) sebelum anda memeroleh pekerjaan pertama? 
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'f6')->textInput() ?>
        </div>
    </div>

    <!-- f7 -->
    <div class="row" id="f7-form-zone">
        <div class="col-md-1"><b>F7</b></div>
        <div class="col-md-5">
            Berapa banyak perusahaan/instansi/institusi yang merespons lamaran anda? 
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'f7')->textInput() ?>
        </div>
    </div>

    <!-- f7A -->
    <div class="row" id="f7a-form-zone">
        <div class="col-md-1"><b>F7A</b></div>
        <div class="col-md-5">
            Berapa banyak perusahaan/instansi/institusi yang mengundang anda untuk wawancara? 
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'f7a')->textInput() ?>
        </div>
    </div>

    <!-- f9 -->
    <div class="row" id="f9-form-zone">
        <div class="col-md-1"><b>F9</b></div>
        <div class="col-md-5">
             Bagaimana anda menggambarkan situasi anda saat ini? <i>Jawaban bisa lebih dari satu</i>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'f901')->checkbox() ?>

            <?= $form->field($model, 'f902')->checkbox() ?>

            <?= $form->field($model, 'f903')->checkbox() ?>

            <?= $form->field($model, 'f904')->checkbox() ?>

            <?= $form->field($model, 'f905')->checkbox() ?>

            <?= $form->field($model, 'f906')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <!-- f10 -->
    <div class="row" id="f10-form-zone">
        <div class="col-md-1"><b>F10</b></div>
        <div class="col-md-5">
            Apakah anda aktif mencari pekerjaan dalam 4 minggu terakhir? Pilihlah Satu Jawaban. KEMUDIAN LANJUT KE f17
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'f1001')->inline(false)->radioList(
            ['1' => '[1] Tidak',
            '2' => '[2] Tidak, tapi saya sedang menunggu hasil lamaran kerja',
            '3' => '[3] Ya, saya akan mulai bekerja dalam 2 minggu ke depan',
            '4' => '[4] Ya, tapi saya belum pasti akan bekerja dalam 2 minggu ke depan',
            '5' => '[5] Lainnya'
            ]) ?>
            

            <?= $form->field($model, 'f1002')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <!-- f11 -->
    <div class="row" id="f11-form-zone">
        <div class="col-md-1"><b>F11</b></div>
        <div class="col-md-5">
            Apa jenis perusahaan/instansi/institusi tempat anda bekerja sekarang? 
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'f1101')->inline(false)->radioList(
            ['1' => '[1] Instansi pemerintah (termasuk BUMN)',
            '2' => '[2] Organisasi non-profit/Lembaga Swadaya Masyarakat',
            '3' => '[3] Perusahaan swasta',
            '4' => '[4] Wiraswasta/perusahaan sendiri',
            '5' => '[5] Lainnya, tuliskan:'
            ]) ?>
            <?= $form->field($model, 'f1102')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <!-- f16 -->
    <div class="row" id="f16-form-zone">
        <div class="col-md-1"><b>F16</b></div>
        <div class="col-md-5">
             Jika menurut anda pekerjaan anda saat ini tidak sesuai dengan pendidikan anda, mengapa anda mengambilnya? <i>Jawaban bisa lebih dari satu</i>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'f1601')->checkbox() ?>

            <?= $form->field($model, 'f1602')->checkbox() ?>

            <?= $form->field($model, 'f1603')->checkbox() ?>

            <?= $form->field($model, 'f1604')->checkbox() ?>

            <?= $form->field($model, 'f1605')->checkbox() ?>

            <?= $form->field($model, 'f1606')->checkbox() ?>

            <?= $form->field($model, 'f1607')->checkbox() ?>

            <?= $form->field($model, 'f1608')->checkbox() ?>

            <?= $form->field($model, 'f1609')->checkbox() ?>

            <?= $form->field($model, 'f1610')->checkbox() ?>

            <?= $form->field($model, 'f1611')->checkbox() ?>

            <?= $form->field($model, 'f1612')->checkbox() ?>

            <?= $form->field($model, 'f1613')->checkbox() ?>

            <?= $form->field($model, 'f1614')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <!-- f17 -->
    <div class="row" id="f17-form-zone">
        <div class="col-md-1"><b>F17</b></div>
        <div class="col-md-5">
            Pada saat lulus, pada tingkat mana kompetensi di bawah ini anda kuasai? (A)
            Pada saat ini, pada tingkat mana kompetensi di bawah ini diperlukan dalam pekerjaan? (B) 
        </div>
        <div class="col-md-6">
            <table class="table">
                <colgroup>
                    <col span="5" class="a_side">
                    <col style="background-color:white">
                    <col span="5" class="b_side">
                </colgroup>

                <tr>
                    <th colspan="5">A(Saat Lulus)</th>
                    <th></th>
                    <th colspan="5">B(Saat Ini)</th>
                </tr>

                <tr>
                    <td colspan="3">Sangat Rendah</td>
                    <td colspan="2">Sangat Tinggi</td>
                    <td></td>
                    <td colspan="3">Sangat Rendah</td>
                    <td colspan="2">Sangat Tinggi</td>
                </tr>
                
                <tr>
                    <td>1</td>
                    <td>2</td>
                    <td>3</td>
                    <td>4</td>
                    <td>5</td>
                    <td></td>
                    <td>1</td>
                    <td>2</td>
                    <td>3</td>
                    <td>4</td>
                    <td>5</td>
                </tr>
                <tr>
                    <!-- this under form is generated 5 td -->
                    <?= $form->field($model, 'f1701')->radioList($val_opt, $template_radio)->label(false) ?>
                    <td>Pengetahuan di bidang atau disiplin ilmu anda <b>f17-1 f17-2b</b></td>
                    <?= $form->field($model, 'f1702b')->radioList($val_opt, $template_radio)->label(false) ?>
                </tr>  
                <tr>
                    <!-- this under form is generated 5 td -->
                    <?= $form->field($model, 'f1703')->radioList($val_opt, $template_radio)->label(false) ?>
                    <td>Pengetahuan di luar bidang atau disiplin ilmu anda  <b>f17-3 f17-4b</b></td>
                    <?= $form->field($model, 'f1704b')->radioList($val_opt, $template_radio)->label(false) ?>
                </tr>

                <tr>
                    <!-- this under form is generated 5 td -->
                    <?= $form->field($model, 'f1705')->radioList($val_opt, $template_radio)->label(false) ?>
                    <td>Pengetahuan umum <b>f17-5 f17-6b</b></td>
                    <?= $form->field($model, 'f1706b')->radioList($val_opt, $template_radio)->label(false) ?>
                </tr>

                <tr>
                    <!-- this under form is generated 5 td -->
                    <?= $form->field($model, 'f1705a')->radioList($val_opt, $template_radio)->label(false) ?>
                    <td>Bahasa Inggris <b>f17-5a f17-6ba</b></td>
                    <?= $form->field($model, 'f1706ba')->radioList($val_opt, $template_radio)->label(false) ?>
                </tr>

                <tr>
                    <!-- this under form is generated 5 td -->
                    <?= $form->field($model, 'f1707')->radioList($val_opt, $template_radio)->label(false) ?>
                    <td>Ketrampilan internet <b>f17-7 f17-8b</b></td>
                    <?= $form->field($model, 'f1708b')->radioList($val_opt, $template_radio)->label(false) ?>
                </tr>

                <tr>
                    <!-- this under form is generated 5 td -->
                    <?= $form->field($model, 'f1709')->radioList($val_opt, $template_radio)->label(false) ?>
                    <td>Ketrampilan komputer <b>f17-9 f17-10b</b></td>
                    <?= $form->field($model, 'f1710b')->radioList($val_opt, $template_radio)->label(false) ?>
                </tr>

                <tr>
                    <!-- this under form is generated 5 td -->
                    <?= $form->field($model, 'f1711')->radioList($val_opt, $template_radio)->label(false) ?>
                    <td>Berpikir kritis <b>f17-11 f17-12b</b></td>
                    <?= $form->field($model, 'f1712b')->radioList($val_opt, $template_radio)->label(false) ?>
                </tr>

                <tr>
                    <!-- this under form is generated 5 td -->
                    <?= $form->field($model, 'f1713')->radioList($val_opt, $template_radio)->label(false) ?>
                    <td>Ketrampilan riset <b>f17-13 f17-14b</b></td>
                    <?= $form->field($model, 'f1714b')->radioList($val_opt, $template_radio)->label(false) ?>
                </tr>

                <tr>
                    <!-- this under form is generated 5 td -->
                    <?= $form->field($model, 'f1715')->radioList($val_opt, $template_radio)->label(false) ?>
                    <td>Kemampuan belajar <b>f17-15 f17-16b</b></td>
                    <?= $form->field($model, 'f1716b')->radioList($val_opt, $template_radio)->label(false) ?>
                </tr>

                <tr>
                    <!-- this under form is generated 5 td -->
                    <?= $form->field($model, 'f1717')->radioList($val_opt, $template_radio)->label(false) ?>
                    <td>Kemampuan berkomunikasi <b>f17-17 f17-18b</b></td>
                    <?= $form->field($model, 'f1718b')->radioList($val_opt, $template_radio)->label(false) ?>
                </tr>

                <tr>
                    <!-- this under form is generated 5 td -->
                    <?= $form->field($model, 'f1719')->radioList($val_opt, $template_radio)->label(false) ?>
                    <td>Bekerja di bawah tekanan <b>f17-19 f17-20b</b></td>
                    <?= $form->field($model, 'f1720b')->radioList($val_opt, $template_radio)->label(false) ?>
                </tr>

                <tr>
                    <!-- this under form is generated 5 td -->
                    <?= $form->field($model, 'f1721')->radioList($val_opt, $template_radio)->label(false) ?>
                    <td>Manajemen waktu <b>f17-21 f17-22b</b></td>
                    <?= $form->field($model, 'f1722b')->radioList($val_opt, $template_radio)->label(false) ?>
                </tr>

                <tr>
                    <!-- this under form is generated 5 td -->
                    <?= $form->field($model, 'f1723')->radioList($val_opt, $template_radio)->label(false) ?>
                    <td>Bekerja secara mandiri <b>f17-23 f17-24b</b></td>
                    <?= $form->field($model, 'f1724b')->radioList($val_opt, $template_radio)->label(false) ?>
                </tr>

                <tr>
                    <!-- this under form is generated 5 td -->
                    <?= $form->field($model, 'f1725')->radioList($val_opt, $template_radio)->label(false) ?>
                    <td>Bekerja dalam tim/bekerjasama dengan orang lain <b>f17-25 f17-26b</b></td>
                    <?= $form->field($model, 'f1726b')->radioList($val_opt, $template_radio)->label(false) ?>
                </tr> 

                <tr>
                    <!-- this under form is generated 5 td -->
                    <?= $form->field($model, 'f1727')->radioList($val_opt, $template_radio)->label(false) ?>
                    <td>Kemampuan dalam memecahkan masalah <b>f17-27 f17-28b</b></td>
                    <?= $form->field($model, 'f1728b')->radioList($val_opt, $template_radio)->label(false) ?>
                </tr> 

                <tr>
                    <!-- this under form is generated 5 td -->
                    <?= $form->field($model, 'f1729')->radioList($val_opt, $template_radio)->label(false) ?>
                    <td>Negosiasi <b>f17-29 f17-30b</b></td>
                    <?= $form->field($model, 'f1730b')->radioList($val_opt, $template_radio)->label(false) ?>
                </tr> 

                <tr>
                    <!-- this under form is generated 5 td -->
                    <?= $form->field($model, 'f1731')->radioList($val_opt, $template_radio)->label(false) ?>
                    <td>Kemampuan analisis <b>f17-31 f17-32b</b></td>
                    <?= $form->field($model, 'f1732b')->radioList($val_opt, $template_radio)->label(false) ?>
                </tr> 

                <tr>
                    <!-- this under form is generated 5 td -->
                    <?= $form->field($model, 'f1733')->radioList($val_opt, $template_radio)->label(false) ?>
                    <td>Toleransi <b>f17-33 f17-34b</b></td>
                    <?= $form->field($model, 'f1734b')->radioList($val_opt, $template_radio)->label(false) ?>
                </tr> 

                <tr>
                    <!-- this under form is generated 5 td -->
                    <?= $form->field($model, 'f1735')->radioList($val_opt, $template_radio)->label(false) ?>
                    <td>Kemampuan adaptasi <b>f17-35 f17-36b</b></td>
                    <?= $form->field($model, 'f1736b')->radioList($val_opt, $template_radio)->label(false) ?>
                </tr> 

                <tr>
                    <!-- this under form is generated 5 td -->
                    <?= $form->field($model, 'f1737')->radioList($val_opt, $template_radio)->label(false) ?>
                    <td>Loyalitas<b>f17-37 f17-38b</b></td>
                    <?= $form->field($model, 'f1738')->radioList($val_opt, $template_radio)->label(false) ?>
                </tr> 

                <tr>
                    <!-- this under form is generated 5 td -->
                    <?= $form->field($model, 'f1737a')->radioList($val_opt, $template_radio)->label(false) ?>
                    <td>Integritas <b>f17-37A f17-38ba</b></td>
                    <?= $form->field($model, 'f1738ba')->radioList($val_opt, $template_radio)->label(false) ?>
                </tr> 

                <tr>
                    <!-- this under form is generated 5 td -->
                    <?= $form->field($model, 'f1739')->radioList($val_opt, $template_radio)->label(false) ?>
                    <td>Bekerja dengan orang yang berbeda budaya maupun latar belakang <b>f17-39 f17-40b</b></td>
                    <?= $form->field($model, 'f1740b')->radioList($val_opt, $template_radio)->label(false) ?>
                </tr> 

                <tr>
                    <!-- this under form is generated 5 td -->
                    <?= $form->field($model, 'f1741')->radioList($val_opt, $template_radio)->label(false) ?>
                    <td>Kepemimpinan <b>f17-41 f17-42b</b></td>
                    <?= $form->field($model, 'f1742b')->radioList($val_opt, $template_radio)->label(false) ?>
                </tr> 

                <tr>
                    <!-- this under form is generated 5 td -->
                    <?= $form->field($model, 'f1743')->radioList($val_opt, $template_radio)->label(false) ?>
                    <td>Kemampuan dalam memegang tanggungjawab <b>f17-43 f17-44b</b></td>
                    <?= $form->field($model, 'f1744b')->radioList($val_opt, $template_radio)->label(false) ?>
                </tr>

                <tr>
                    <!-- this under form is generated 5 td -->
                    <?= $form->field($model, 'f1745')->radioList($val_opt, $template_radio)->label(false) ?>
                    <td>Inisiatif <b>f17-45 f17-46b</b></td>
                    <?= $form->field($model, 'f1746b')->radioList($val_opt, $template_radio)->label(false) ?>
                </tr> 

                <tr>
                    <!-- this under form is generated 5 td -->
                    <?= $form->field($model, 'f1747')->radioList($val_opt, $template_radio)->label(false) ?>
                    <td>Manajemen proyek/program <b>f17-47 f17-48b</b></td>
                    <?= $form->field($model, 'f1748b')->radioList($val_opt, $template_radio)->label(false) ?>
                </tr> 

                <tr>
                    <!-- this under form is generated 5 td -->
                    <?= $form->field($model, 'f1749')->radioList($val_opt, $template_radio)->label(false) ?>
                    <td>Kemampuan untuk memresentasikan ide/produk/laporan <b>f17-49 f17-50b</b></td>
                    <?= $form->field($model, 'f1750b')->radioList($val_opt, $template_radio)->label(false) ?>
                </tr> 

                <tr>
                    <!-- this under form is generated 5 td -->
                    <?= $form->field($model, 'f1751')->radioList($val_opt, $template_radio)->label(false) ?>
                    <td>Kemampuan dalam menulis laporan, memo dan dokumen <b>f17-51 f17-52b</b></td>
                    <?= $form->field($model, 'f1752b')->radioList($val_opt, $template_radio)->label(false) ?>
                </tr>

                <tr>
                    <!-- this under form is generated 5 td -->
                    <?= $form->field($model, 'f1753')->radioList($val_opt, $template_radio)->label(false) ?>
                    <td>Kemampuan untuk terus belajar sepanjang hayat <b>f17-53 f17-54b</b></td>
                    <?= $form->field($model, 'f1754b')->radioList($val_opt, $template_radio)->label(false) ?>
                </tr>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-1"><b></b></div>
        <div class="col-md-5">
        </div>
        <div class="col-md-6">
            <?php if(Yii::$app->user->identity->type != 'admin'){ ?>
                <?= $form->field($model, 'f_download')->hiddenInput(['maxlength' => true])->label(false) ?>
            <?php } else { ?>
                <?= $form->field($model, 'f_download')->dropdownList(['0' => 'Belum Terdownload', '1' => 'Telah Terdownload']) ?>
            <?php } ?>

            <div class="form-group">
                <?= Html::submitButton('Simpan', ['class' => 'btn btn-block btn-success']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php 
$jscript =<<<JS
//f501
$('{$first_objHide_f5x}').hide();
$('.field-surveytracert-f1202').hide();
$('#surveytracert-f502, #surveytracert-f503').val(0);

function hide1()
{
    //jika tidak bekerja
    //hide f9, f10, f11, f12, f13, f14, f15, f16 
    $('#f9-form-zone, #f10-form-zone, #f11-form-zone, #f12-form-zone, #f13-form-zone, #f14-form-zone, #f15-form-zone, #f16-form-zone').hide(1000);
}

function show1()
{
    //jika bekerja
    //show f9, f10, f11, f12, f13, f14, f15, f16
    $('#f9-form-zone, #f10-form-zone, #f11-form-zone, #f12-form-zone, #f13-form-zone, #f14-form-zone, #f15-form-zone, #f16-form-zone').show(1000);
}

function hide2()
{
    //hide f11, f12, f13, f14, f15, f16
    $('#f11-form-zone, #f12-form-zone, #f13-form-zone, #f14-form-zone, #f15-form-zone, #f16-form-zone').hide(1000);
}

function show2()
{
    //show f11, f12, f13, f14, f15, f16
    $('#f11-form-zone, #f12-form-zone, #f13-form-zone, #f14-form-zone, #f15-form-zone, #f16-form-zone').show(1000);
}

$('input[name=\'SurveyTracert[f8]\']').click(function(){
    var i = $(this).val();
    if(i == "1"){
        show1();
    } else {//2
        hide1();
    }
});

$('input[name=\'SurveyTracert[f301]\']').click(function(){
    var i = $(this).val();
    if(i == "3"){
        hide2();
    } else {//2
        show2();
    }
});

$('input[name=\'SurveyTracert[f1001]\']').click(function(){
    var i = $(this).val();
    hide2();
});

//f5
$('input[name=\'SurveyTracert[f501]\']').click(function(){
    var i = $(this).val();
    if(i == "1"){
        $('.field-surveytracert-f502').show();
        $('.field-surveytracert-f503').hide();
        $('#surveytracert-f502').val(1);
        $('#surveytracert-f503').val(0);
    } else {
        $('.field-surveytracert-f502').hide();
        $('.field-surveytracert-f503').show();
        $('#surveytracert-f502').val(0);
        $('#surveytracert-f503').val(1);
    }
});

//f12
$('input[name=\'SurveyTracert[f1201]\']').click(function(){
    var i = $(this).val();
    if(i == "7"){
        $('.field-surveytracert-f1202').show();
        $('#surveytracert-f1202').val("-");
    } else {
        $('.field-surveytracert-f1202').hide();
        $('#surveytracert-f1202').val('-');
    }
});

//f3

JS;

$this->registerJs($jscript);
