<?php
namespace frontend\modules\survey\models;
// use frontend\modules\survey\models\SurveyTracert;

use Yii;
use yii\db\ActiveRecord;
use frontend\modules\profile\models\UserProfile;
use common\models\User;

/**
 * This is the model class for table "survey_tracert".
 *
 * @property string $nimhsmsmh
 * @property string $f501
 * @property string $f502
 * @property string $f503
 * @property string $f1201
 * @property string $f1202
 * @property string $f8
 * @property string $f14
 * @property string $f15
 * @property string $f1301
 * @property string $f1302
 * @property string $f1303
 * @property string $f21
 * @property string $f22
 * @property string $f23
 * @property string $f24
 * @property string $f25
 * @property string $f26
 * @property string $f27
 * @property string $f301
 * @property string $f302
 * @property string $f303
 * @property string $f401
 * @property string $f402
 * @property string $f403
 * @property string $f404
 * @property string $f405
 * @property string $f406
 * @property string $f407
 * @property string $f408
 * @property string $f409
 * @property string $f410
 * @property string $f411
 * @property string $f412
 * @property string $f413
 * @property string $f414
 * @property string $f415
 * @property int $f6
 * @property int $f7
 * @property int $f7a
 * @property string $f901
 * @property string $f902
 * @property string $f903
 * @property string $f904
 * @property string $f905
 * @property string $f906
 * @property string $f1001
 * @property string $f1002
 * @property string $f1101
 * @property string $f1102
 * @property string $f1601
 * @property string $f1602
 * @property string $f1603
 * @property string $f1604
 * @property string $f1605
 * @property string $f1606
 * @property string $f1607
 * @property string $f1608
 * @property string $f1609
 * @property string $f1610
 * @property string $f1611
 * @property string $f1612
 * @property string $f1613
 * @property string $f1614
 * @property string $f1701
 * @property string $f1702b
 * @property string $f1703
 * @property string $f1704b
 * @property string $f1705
 * @property string $f1706b
 * @property string $f1705a
 * @property string $f1706ba
 * @property string $f1707
 * @property string $f1708b
 * @property string $f1709
 * @property string $f1710b
 * @property string $f1711
 * @property string $f1712b
 * @property string $f1713
 * @property string $f1714b
 * @property string $f1715
 * @property string $f1716b
 * @property string $f1717
 * @property string $f1718b
 * @property string $f1719
 * @property string $f1720b
 * @property string $f1721
 * @property string $f1722b
 * @property string $f1723
 * @property string $f1724b
 * @property string $f1725
 * @property string $f1726b
 * @property string $f1727
 * @property string $f1728b
 * @property string $f1729
 * @property string $f1730b
 * @property string $f1731
 * @property string $f1732b
 * @property string $f1733
 * @property string $f1734b
 * @property string $f1735
 * @property string $f1736b
 * @property string $f1737
 * @property string $f1738b
 * @property string $f1737a
 * @property string $f1738ba
 * @property string $f1739
 * @property string $f1740b
 * @property string $f1741
 * @property string $f1742b
 * @property string $f1743
 * @property string $f1744b
 * @property string $f1745
 * @property string $f1746b
 * @property string $f1747
 * @property string $f1748b
 * @property string $f1749
 * @property string $f1750b
 * @property string $f1751
 * @property string $f1752b
 * @property string $f1753
 * @property string $f1754b
 * @property int $created_at
 * @property int $updated_at
 */
class SurveyTracert extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'survey_tracert';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nimhsmsmh', 'f501', 'f1201', 'f8'], 'required'],
            [['f14', 'f15', 'f1301', 'f1302', 'f1303'], 'required', 'when' => function($model){
                return $model->f8 == '1';
            }],
            ['f502', 'required', 'when' => function($model){
                return $model->f501 == '1';
            }],
            ['f503', 'required', 'when' => function($model){
                return $model->f501 == '2';
            }],
            ['f1202', 'required', 'when' => function($model){
                return $model->f1201 == '7';
            }],
            [['f1301', 'f1302', 'f1303'], 'integer'],
            [['f6', 'f7', 'f7a', 'f_download'], 'integer'],
            [['nimhsmsmh'], 'string', 'max' => 15],
            [['f501', 'f1201', 'f8', 'f14', 'f15', 'f21', 'f22', 'f23', 'f24', 'f25', 'f26', 'f27', 'f301', 'f401', 'f402', 'f403', 'f404', 'f405', 'f406', 'f407', 'f408', 'f409', 'f410', 'f411', 'f412', 'f413', 'f414', 'f415', 'f901', 'f902', 'f903', 'f904', 'f905', 'f1001', 'f1101', 'f1701', 'f1702b', 'f1703', 'f1704b', 'f1705', 'f1706b', 'f1705a', 'f1706ba', 'f1707', 'f1708b', 'f1709', 'f1710b', 'f1711', 'f1711a', 'f1712a', 'f1712b', 'f1713', 'f1714b', 'f1715', 'f1716b', 'f1717', 'f1718b', 'f1719', 'f1720b', 'f1721', 'f1722b', 'f1723', 'f1724b', 'f1725', 'f1726b', 'f1727', 'f1728b', 'f1729', 'f1730b', 'f1731', 'f1732b', 'f1733', 'f1734b', 'f1735', 'f1736b', 'f1737', 'f1738', 'f1737a', 'f1738ba', 'f1739', 'f1740b', 'f1741', 'f1742b', 'f1743', 'f1744b', 'f1745', 'f1746b', 'f1747', 'f1748b', 'f1749', 'f1750b', 'f1751', 'f1752b', 'f1753', 'f1754b', 'f1706'], 'string', 'max' => 1],
            [['f502', 'f503'], 'string', 'max' => 3],
            [['f1202', 'f1614', 'f416'], 'string', 'max' => 200],
            [['f302', 'f303', 'f1601', 'f1602', 'f1603', 'f1604', 'f1605', 'f1606', 'f1607', 'f1608', 'f1609', 'f1610', 'f1611', 'f1612', 'f1613'], 'string', 'max' => 2],
            [['f906', 'f1002', 'f1102'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nimhsmsmh' => 'NPM',
            'f501' => '(F501)',
            'f502' => '(F502)',
            'f503' => '(F503)',

            'f1201' => '(F1201)',
            'f1202' => '(F1202)',

            'f8' => '(F8)',
            'f14' => '(F14)',
            'f15' => '(F15)',

            'f1301' => 'Dari Pekerjaan Utama (F1301)',
            'f1302' => 'Dari Lembur dan Tips (F1302)',
            'f1303' => 'Dari Pekerjaan Lainnya (F1303)',

            'f21' => 'Perkuliahan (F21)',
            'f22' => 'Demonstrasi (F22)',
            'f23' => 'Partisipasi dalam proyek riset(F23)',
            'f24' => 'Magang (F24)',
            'f25' => 'Praktikum (F25)',
            'f26' => 'Kerja Lapangan(F26)',
            'f27' => 'Diskusi (F27)',

            'f301' => '(F301)',
            'f302' => 'Kira-kira bulan sebelum lulus(F302)',
            'f303' => 'Kira-kira bulan sesudah lulus(F303)',

            'f401' => '[1] Melalui iklan di koran/majalah, brosur(F401)',
            'f402' => '[1] Melamar ke perusahaan tanpa mengetahui lowongan yang ada(F402)',
            'f403' => '[1] Pergi ke bursa/pameran kerja(F403)',
            'f404' => '[1] Mencari lewat internet/iklan online/milis(F404)',
            'f405' => '[1] Dihubungi oleh perusahaan(F405)',
            'f406' => '[1] Menghubungi Kemenakertrans(F406)',
            'f407' => '[1] Menghubungi agen tenaga kerja komersial/swasta(F407)',
            'f408' => '[1] Memeroleh informasi dari pusat/kantor pengembangan karir fakultas/universitas(F408)',
            'f409' => '[1] Menghubungi kantor kemahasiswaan/hubungan alumni(F409)',
            'f410' => '[1] Membangun jejaring (network) sejak masih kuliah(F410)',
            'f411' => '[1] Melalui relasi (misalnya dosen, orang tua, saudara, teman, dll.)(F411)',
            'f412' => '[1] Membangun bisnis sendiri(F412)',
            'f413' => '[1] Melalui penempatan kerja atau magang(F413)',
            'f414' => '[1] Bekerja di tempat yang sama dengan tempat kerja semasa kuliah(F414)',
            'f415' => '[1] Lainnya:(F415)',
            'f416' => 'Uraian F415 (F416)',

            'f6' => '(F6)',
            'f7' => '(F7)',
            'f7a' => '(F7a)',

            'f901' => '[1] Saya masih belajar/melanjutkan kuliah profesi atau pascasarjana(F901)',
            'f902' => '[2] Saya menikah(F902)',
            'f903' => '[3] Saya sibuk dengan keluarga dan anak-anak(F903)',
            'f904' => '[4] Saya sekarang sedang mencari pekerjaan (F904)',
            'f905' => '[5] Lainnya(F905)',
            'f906' => 'Uraian (F905) (F906)',
            'f1001' => '(F10-01)',
            'f1002' => 'Uraian F10-01 (F10-02)',
            'f1101' => '(F1101)',
            'f1102' => 'Uraian F11-01(F1102)',
            'f1601' => '[1] Pertanyaan tidak sesuai; pekerjaan saya sekarang sudah sesuai dengan pendidikan saya. (F1601)',
            'f1602' => '[2] Saya belum mendapatkan pekerjaan yang lebih sesuai. (F1602)',
            'f1603' => '[3] Di pekerjaan ini saya memeroleh prospek karir yang baik. (F1603)',
            'f1604' => '[4] Saya lebih suka bekerja di area pekerjaan yang tidak ada hubungannya dengan pendidikan saya. (F1604)',
            'f1605' => '[5] Saya dipromosikan ke posisi yang kurang berhubungan dengan pendidikan saya dibanding posisi sebelumnya.(F1605)',
            'f1606' => '[6] Saya dapat memeroleh pendapatan yang lebih tinggi di pekerjaan ini.(F16-06)',
            'f1607' => '[7] Pekerjaan saya saat ini lebih aman/terjamin/secure(F16-07)',
            'f1608' => '[8] Pekerjaan saya saat ini lebih menarik (F16-08)',
            'f1609' => '[9] Pekerjaan saya saat ini lebih memungkinkan saya mengambil pekerjaan tambahan/jadwal yang fleksibel, dll.(F16-09)',
            'f1610' => '[10] Pekerjaan saya saat ini lokasinya lebih dekat dari rumah saya.(F16-10)',
            'f1611' => '[11] Pekerjaan saya saat ini dapat lebih menjamin kebutuhan keluarga saya.(F16-11)',
            'f1612' => '[12] Pada awal meniti karir ini, saya harus menerima pekerjaan yang tidak berhubungan dengan pendidikan saya.(F16-12)',
            'f1613' => '[13] Lainnya: (F16-13)',
            'f1614' => 'Uraian: (F16-13) (F16-14)',
            'f1701' => '(F1701)',
            'f1702b' => '(F1702b)',
            'f1703' => '(F1703)',
            'f1704b' => '(F1704b)',
            'f1705' => '(F1705)',
            'f1706b' => '(F1706b)',
            'f1705a' => '(F1705a)',
            'f1706ba' => '(F1706ba)',
            'f1707' => '(F1707)',
            'f1708b' => '(F1708b)',
            'f1709' => '(F1709)',
            'f1710b' => '(F1710b)',
            'f1711' => '(F1711)',
            'f1712b' => '(F1712b)',
            'f1713' => '(F1713)',
            'f1714b' => '(F1714b)',
            'f1715' => '(F1715)',
            'f1716b' => '(F1716b)',
            'f1717' => '(F1717)',
            'f1718b' => '(F1718b)',
            'f1719' => '(F1719)',
            'f1720b' => '(F1720b)',
            'f1721' => '(F1721)',
            'f1722b' => '(F1722b)',
            'f1723' => '(F1723)',
            'f1724b' => '(F1724b)',
            'f1725' => '(F1725)',
            'f1726b' => '(F1726b)',
            'f1727' => '(F1727)',
            'f1728b' => '(F1728b)',
            'f1729' => '(F1729)',
            'f1730b' => '(F1730b)',
            'f1731' => '(F1731)',
            'f1732b' => '(F1732b)',
            'f1733' => '(F1733)',
            'f1734b' => '(F1734b)',
            'f1735' => '(F1735)',
            'f1736b' => '(F1736b)',
            'f1737' => '(F1737)',
            'f1738b' => '(F1738b)',
            'f1737a' => '(F1737a)',
            'f1738ba' => '(F1738ba)',
            'f1739' => '(F1739)',
            'f1740b' => '(F1740b)',
            'f1741' => '(F1741)',
            'f1742b' => '(F1742b)',
            'f1743' => '(F1743)',
            'f1744b' => '(F1744b)',
            'f1745' => '(F1745)',
            'f1746b' => '(F1746b)',
            'f1747' => '(F1747)',
            'f1748b' => '(F1748b)',
            'f1749' => '(F1749)',
            'f1750b' => '(F1750b)',
            'f1751' => '(F1751)',
            'f1752b' => '(F1752b)',
            'f1753' => '(F1753)',
            'f1754b' => '(F1754b)',
            'f_download' => 'Status Download',
            'created_at' => 'Tanggal Dibuat',
            'updated_at' => 'Tanggal Diupdate',
        ];
    }

    public function getUserone()
    {
        return $this->hasOne(User::className(), ['username' => 'nimhsmsmh']);
    }

    public function getProfile()
    {
        return $this->hasOne(UserProfile::className(), ['nimhsmsmh' => 'nimhsmsmh']);
    }
}
