<?php
namespace frontend\modules\survey\controllers;

use Yii;
use frontend\modules\survey\models\SurveyTracert;
use frontend\modules\profile\models\UserProfile;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
 * SurveyTracertController implements the CRUD actions for SurveyTracert model.
 */
class SurveyTracertController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'me', 'view', 'export', 'export2'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SurveyTracert models.
     * @return mixed
     */
    public function actionIndex()
    {
  		//just admin
    	if(Yii::$app->user->identity->type != 'admin'){
    		return $this->redirect(['/site/index']);
    	}

        $dataProvider = new ActiveDataProvider([
            'query' => SurveyTracert::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionMe()
    {
        $check = UserProfile::findOne(['nimhsmsmh' => Yii::$app->user->identity->username]);
        if($check == null){
            return $this->redirect(['/profile/userprofile/me']);
        }

        $check = SurveyTracert::findOne(['nimhsmsmh' => Yii::$app->user->identity->username]);
        if($check == null){
            return $this->redirect(['create']);
        }

        return $this->render('view', [
            'model' => $this->findModel(Yii::$app->user->identity->username),
        ]);
    }

    public function actionExport()
    {
        $fxls ='export/tracert-study-res_'.time();
        
        $dataProvider = SurveyTracert::find()->where(['f_download' => 0]);

        if($dataProvider->count() == 0)
        {
            return $this->redirect('index');
        }

        //object of the Spreadsheet class to create the excel data
        $spreadsheet = new Spreadsheet();

        //set style for A1,B1,C1 cells
        $cell_st =[
         'font' =>['bold' => true],
         'alignment' =>['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER],
         'borders'=>['bottom' =>['style'=> \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM]]
        ];
        
        //add some data in excel cells
        $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A1', 'kdptimsmh')
        ->setCellValue('B1', 'kdpstmsmh')
        ->setCellValue('C1', 'nimhsmsmh')
        ->setCellValue('D1', 'nmmhsmsmh')
        ->setCellValue('E1', 'telpomsmh')
        ->setCellValue('F1', 'emailmsmh')
        ->setCellValue('G1', 'tahun_lulus')
        ->setCellValue('H1', 'f21')
        ->setCellValue('I1', 'f22')
        ->setCellValue('J1', 'f23')
        ->setCellValue('K1', 'f24')
        ->setCellValue('L1', 'f25')
        ->setCellValue('M1', 'f26')
        ->setCellValue('N1', 'f27')
        ->setCellValue('O1', 'f301')
        ->setCellValue('P1', 'f302 ')
        ->setCellValue('Q1', 'f303 ')
        ->setCellValue('R1', 'f401')
        ->setCellValue('S1', 'f402')
        ->setCellValue('T1', 'f403')
        ->setCellValue('U1', 'f404')
        ->setCellValue('V1', 'f405')
        ->setCellValue('W1', 'f406')
        ->setCellValue('X1', 'f407')
        ->setCellValue('Y1', 'f408')
        ->setCellValue('Z1', 'f409')
        ->setCellValue('AA1', 'f410')
        ->setCellValue('AB1', 'f411')
        ->setCellValue('AC1', 'f412')
        ->setCellValue('AD1', 'f413')
        ->setCellValue('AE1', 'f414')
        ->setCellValue('AF1', 'f415')
        ->setCellValue('AG1', 'f416')
        ->setCellValue('AH1', 'f6 ')
        ->setCellValue('AI1', 'f501')
        ->setCellValue('AJ1', 'f502 ')
        ->setCellValue('AK1', 'f503 ')
        ->setCellValue('AL1', 'f7 ')
        ->setCellValue('AM1', 'f7a')
        ->setCellValue('AN1', 'f8')
        ->setCellValue('AO1', 'f901')
        ->setCellValue('AP1', 'f902')
        ->setCellValue('AQ1', 'f903')
        ->setCellValue('AR1', 'f904')
        ->setCellValue('AS1', 'f905')
        ->setCellValue('AT1', 'f906')
        ->setCellValue('AU1', 'f1001')
        ->setCellValue('AV1', 'f1002')
        ->setCellValue('AW1', 'f1101')
        ->setCellValue('AX1', 'f1102')
        ->setCellValue('AY1', 'f1201')
        ->setCellValue('AZ1', 'f1202')
        ->setCellValue('BA1', 'f1301 ')
        ->setCellValue('BB1', 'f1302 ')
        ->setCellValue('BC1', 'f1303 ')
        ->setCellValue('BD1', 'f14')
        ->setCellValue('BE1', 'f15')
        ->setCellValue('BF1', 'f1601')
        ->setCellValue('BG1', 'f1602')
        ->setCellValue('BH1', 'f1603')
        ->setCellValue('BI1', 'f1604')
        ->setCellValue('BJ1', 'f1605')
        ->setCellValue('BK1', 'f1606')
        ->setCellValue('BL1', 'f1607')
        ->setCellValue('BM1', 'f1608')
        ->setCellValue('BN1', 'f1609')
        ->setCellValue('BO1', 'f1610')
        ->setCellValue('BP1', 'f1611')
        ->setCellValue('BQ1', 'f1612')
        ->setCellValue('BR1', 'f1613')
        ->setCellValue('BS1', 'f1614');

        $startPoint = 2;

        foreach($dataProvider->all() as $v){
            $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A'.$startPoint, $v->profile->kdptimsmh)
            ->setCellValue('B'.$startPoint, $v->profile->kdpstmsmh)
            ->setCellValue('C'.$startPoint, $v->profile->nimhsmsmh)
            ->setCellValue('D'.$startPoint, $v->profile->nmmhsmsmh)
            ->setCellValue('E'.$startPoint, $v->profile->telpomsmh)
            ->setCellValue('F'.$startPoint, $v->profile->emailmsmh)
            ->setCellValue('G'.$startPoint, $v->profile->tahun_lulus)
            ->setCellValue('H'.$startPoint, $v->f21)
            ->setCellValue('I'.$startPoint, $v->f22)
            ->setCellValue('J'.$startPoint, $v->f23)
            ->setCellValue('K'.$startPoint, $v->f24)
            ->setCellValue('L'.$startPoint, $v->f25)
            ->setCellValue('M'.$startPoint, $v->f26)
            ->setCellValue('N'.$startPoint, $v->f27)
            ->setCellValue('O'.$startPoint, $v->f301)
            ->setCellValue('P'.$startPoint, $v->f302)
            ->setCellValue('Q'.$startPoint, $v->f303)
            ->setCellValue('R'.$startPoint, $v->f401)
            ->setCellValue('S'.$startPoint, $v->f402)
            ->setCellValue('T'.$startPoint, $v->f403)
            ->setCellValue('U'.$startPoint, $v->f404)
            ->setCellValue('V'.$startPoint, $v->f405)
            ->setCellValue('W'.$startPoint, $v->f406)
            ->setCellValue('X'.$startPoint, $v->f407)
            ->setCellValue('Y'.$startPoint, $v->f408)
            ->setCellValue('Z'.$startPoint, $v->f409)
            ->setCellValue('AA'.$startPoint, $v->f410)
            ->setCellValue('AB'.$startPoint, $v->f411)
            ->setCellValue('AC'.$startPoint, $v->f412)
            ->setCellValue('AD'.$startPoint, $v->f413)
            ->setCellValue('AE'.$startPoint, $v->f414)
            ->setCellValue('AF'.$startPoint, $v->f415)
            ->setCellValue('AG'.$startPoint, $v->f416)
            ->setCellValue('AH'.$startPoint, $v->f6)
            ->setCellValue('AI'.$startPoint, $v->f501)
            ->setCellValue('AJ'.$startPoint, $v->f502)
            ->setCellValue('AK'.$startPoint, $v->f503)
            ->setCellValue('AL'.$startPoint, $v->f7)
            ->setCellValue('AM'.$startPoint, $v->f7a)
            ->setCellValue('AN'.$startPoint, $v->f8)
            ->setCellValue('AO'.$startPoint, $v->f901)
            ->setCellValue('AP'.$startPoint, $v->f902)
            ->setCellValue('AQ'.$startPoint, $v->f903)
            ->setCellValue('AR'.$startPoint, $v->f904)
            ->setCellValue('AS'.$startPoint, $v->f905)
            ->setCellValue('AT'.$startPoint, $v->f906)
            ->setCellValue('AU'.$startPoint, $v->f1001)
            ->setCellValue('AV'.$startPoint, $v->f1002)
            ->setCellValue('AW'.$startPoint, $v->f1101)
            ->setCellValue('AX'.$startPoint, $v->f1102)
            ->setCellValue('AY'.$startPoint, $v->f1201)
            ->setCellValue('AZ'.$startPoint, $v->f1202)
            ->setCellValue('BA'.$startPoint, $v->f1301)
            ->setCellValue('BB'.$startPoint, $v->f1302)
            ->setCellValue('BC'.$startPoint, $v->f1303)
            ->setCellValue('BD'.$startPoint, $v->f14)
            ->setCellValue('BE'.$startPoint, $v->f15)
            ->setCellValue('BF'.$startPoint, $v->f1601)
            ->setCellValue('BG'.$startPoint, $v->f1602)
            ->setCellValue('BH'.$startPoint, $v->f1603)
            ->setCellValue('BI'.$startPoint, $v->f1604)
            ->setCellValue('BJ'.$startPoint, $v->f1605)
            ->setCellValue('BK'.$startPoint, $v->f1606)
            ->setCellValue('BL'.$startPoint, $v->f1607)
            ->setCellValue('BM'.$startPoint, $v->f1608)
            ->setCellValue('BN'.$startPoint, $v->f1609)
            ->setCellValue('BO'.$startPoint, $v->f1610)
            ->setCellValue('BP'.$startPoint, $v->f1611)
            ->setCellValue('BQ'.$startPoint, $v->f1612)
            ->setCellValue('BR'.$startPoint, $v->f1613)
            ->setCellValue('BS'.$startPoint, $v->f1614);

            $startPoint++;
        }

        $spreadsheet->getActiveSheet()->getStyle('A1:BS1')->applyFromArray($cell_st);
        
        //set columns width
        /*$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(16);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);*/

        $spreadsheet->getActiveSheet()->setTitle('Tracert'); //set a title for Worksheet
        
        //make object of the Xlsx class to save the excel file
        $writer = new Xlsx($spreadsheet);
        
        $writer->save($fxls.'-1.xlsx');

        /*--------------------------new */

        $newss = new Spreadsheet();

        $newss->setActiveSheetIndex(0)
        ->setCellValue('A1', 'f1701')
        ->setCellValue('B1', 'f1702b')
        ->setCellValue('C1', 'f1703')
        ->setCellValue('D1', 'f1704b')
        ->setCellValue('E1', 'f1705')
        ->setCellValue('F1', 'f1705a')
        ->setCellValue('G1', 'f1706')
        ->setCellValue('H1', 'f1706ba')
        ->setCellValue('I1', 'f1707')
        ->setCellValue('J1', 'f1708b')
        ->setCellValue('K1', 'f1709')
        ->setCellValue('L1', 'f1710b')
        ->setCellValue('M1', 'f1711')
        ->setCellValue('N1', 'f1711a')
        ->setCellValue('O1', 'f1712b')
        ->setCellValue('P1', 'f1712a')
        ->setCellValue('Q1', 'f1713')
        ->setCellValue('R1', 'f1714b')
        ->setCellValue('S1', 'f1715')
        ->setCellValue('T1', 'f1716b')
        ->setCellValue('U1', 'f1717')
        ->setCellValue('V1', 'f1718b')
        ->setCellValue('W1', 'f1719')
        ->setCellValue('X1', 'f1720b')
        ->setCellValue('Y1', 'f1721')
        ->setCellValue('Z1', 'f1722b')
        ->setCellValue('AA1', 'f1723')
        ->setCellValue('AB1', 'f1724b')
        ->setCellValue('AC1', 'f1725')
        ->setCellValue('AD1', 'f1726b')
        ->setCellValue('AE1', 'f1727')
        ->setCellValue('AF1', 'f1728b')
        ->setCellValue('AG1', 'f1729')
        ->setCellValue('AH1', 'f1730b')
        ->setCellValue('AI1', 'f1731')
        ->setCellValue('AJ1', 'f1732b')
        ->setCellValue('AK1', 'f1733')
        ->setCellValue('AL1', 'f1734b')
        ->setCellValue('AM1', 'f1735')
        ->setCellValue('AN1', 'f1736b')
        ->setCellValue('AO1', 'f1737')
        ->setCellValue('AP1', 'f1737a')
        ->setCellValue('AQ1', 'f1738')
        ->setCellValue('AR1', 'f1738ba')
        ->setCellValue('AS1', 'f1739')
        ->setCellValue('AT1', 'f1740b')
        ->setCellValue('AU1', 'f1741')
        ->setCellValue('AV1', 'f1742b')
        ->setCellValue('AW1', 'f1743')
        ->setCellValue('AX1', 'f1744b')
        ->setCellValue('AY1', 'f1745')
        ->setCellValue('AZ1', 'f1746b')
        ->setCellValue('BA1', 'f1747')
        ->setCellValue('BB1', 'f1748b')
        ->setCellValue('BC1', 'f1749')
        ->setCellValue('BD1', 'f1750b')
        ->setCellValue('BE1', 'f1751')
        ->setCellValue('BF1', 'f1752b')
        ->setCellValue('BG1', 'f1753')
        ->setCellValue('BH1', 'f1754b');

        $startPoint = 2;

        foreach($dataProvider->all() as $v){
            $newss->setActiveSheetIndex(0)
            ->setCellValue('A'.$startPoint, $v->f1701)
            ->setCellValue('B'.$startPoint, $v->f1702b)
            ->setCellValue('C'.$startPoint, $v->f1703)
            ->setCellValue('D'.$startPoint, $v->f1704b)
            ->setCellValue('E'.$startPoint, $v->f1705)
            ->setCellValue('F'.$startPoint, $v->f1705a)
            ->setCellValue('G'.$startPoint, $v->f1706)
            ->setCellValue('H'.$startPoint, $v->f1706ba)
            ->setCellValue('I'.$startPoint, $v->f1707)
            ->setCellValue('J'.$startPoint, $v->f1708b)
            ->setCellValue('K'.$startPoint, $v->f1709)
            ->setCellValue('L'.$startPoint, $v->f1710b)
            ->setCellValue('M'.$startPoint, $v->f1711)
            ->setCellValue('N'.$startPoint, $v->f1711a)
            ->setCellValue('O'.$startPoint, $v->f1712b)
            ->setCellValue('P'.$startPoint, $v->f1712a)
            ->setCellValue('Q'.$startPoint, $v->f1713)
            ->setCellValue('R'.$startPoint, $v->f1714b)
            ->setCellValue('S'.$startPoint, $v->f1715)
            ->setCellValue('T'.$startPoint, $v->f1716b)
            ->setCellValue('U'.$startPoint, $v->f1717)
            ->setCellValue('V'.$startPoint, $v->f1718b)
            ->setCellValue('W'.$startPoint, $v->f1719)
            ->setCellValue('X'.$startPoint, $v->f1720b)
            ->setCellValue('Y'.$startPoint, $v->f1721)
            ->setCellValue('Z'.$startPoint, $v->f1722b)
            ->setCellValue('AA'.$startPoint, $v->f1723)
            ->setCellValue('AB'.$startPoint, $v->f1724b)
            ->setCellValue('AC'.$startPoint, $v->f1725)
            ->setCellValue('AD'.$startPoint, $v->f1726b)
            ->setCellValue('AE'.$startPoint, $v->f1727)
            ->setCellValue('AF'.$startPoint, $v->f1728b)
            ->setCellValue('AG'.$startPoint, $v->f1729)
            ->setCellValue('AH'.$startPoint, $v->f1730b)
            ->setCellValue('AI'.$startPoint, $v->f1731)
            ->setCellValue('AJ'.$startPoint, $v->f1732b)
            ->setCellValue('AK'.$startPoint, $v->f1733)
            ->setCellValue('AL'.$startPoint, $v->f1734b)
            ->setCellValue('AM'.$startPoint, $v->f1735)
            ->setCellValue('AN'.$startPoint, $v->f1736b)
            ->setCellValue('AO'.$startPoint, $v->f1737)
            ->setCellValue('AP'.$startPoint, $v->f1737a)
            ->setCellValue('AQ'.$startPoint, $v->f1738)
            ->setCellValue('AR'.$startPoint, $v->f1738ba)
            ->setCellValue('AS'.$startPoint, $v->f1739)
            ->setCellValue('AT'.$startPoint, $v->f1740b)
            ->setCellValue('AU'.$startPoint, $v->f1741)
            ->setCellValue('AV'.$startPoint, $v->f1742b)
            ->setCellValue('AW'.$startPoint, $v->f1743)
            ->setCellValue('AX'.$startPoint, $v->f1744b)
            ->setCellValue('AY'.$startPoint, $v->f1745)
            ->setCellValue('AZ'.$startPoint, $v->f1746b)
            ->setCellValue('BA'.$startPoint, $v->f1747)
            ->setCellValue('BB'.$startPoint, $v->f1748b)
            ->setCellValue('BC'.$startPoint, $v->f1749)
            ->setCellValue('BD'.$startPoint, $v->f1750b)
            ->setCellValue('BE'.$startPoint, $v->f1751)
            ->setCellValue('BF'.$startPoint, $v->f1752b)
            ->setCellValue('BG'.$startPoint, $v->f1753)
            ->setCellValue('BH'.$startPoint, $v->f1754b);

            $startPoint++;
        }

        $newss->getActiveSheet()->getStyle('A1:BH1')->applyFromArray($cell_st);
        
        //set columns width
        /*$spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(16);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);*/

        $newss->getActiveSheet()->setTitle('Tracert 2'); //set a title for Worksheet
        
        //make object of the Xlsx class to save the excel file
        $newwriter = new Xlsx($newss);
        
        $newwriter->save($fxls.'-2.xlsx');
        
        /*$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        $writer2 = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet2, "Xlsx");
        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$fxls.'"');
        // download file 
        $writer->save("php://output");*/

        //make part 1 and 2 then zipping
        $zip = new \ZipArchive();
        if ($zip->open('./'.$fxls.'.zip', \ZipArchive::CREATE)) {
          // FILE NAME, STRING TO ADD
        // addFile -> nama file, namafile dalam zip
            $zip->addFile('./'.$fxls.'-1.xlsx', 'file-1.xlsx');
            $zip->addFile('./'.$fxls.'-2.xlsx', 'file-2.xlsx');
            $zip->close();

            unlink('./'.$fxls.'-1.xlsx');
            unlink('./'.$fxls.'-2.xlsx');

            SurveyTracert::updateAll(['f_download' => 1]);
            return $this->redirect(['/'.$fxls.'.zip']);
        } else { 
            return $this->redirect(['index']);
        }

    }

    public function actionExport2()
    {
        $dataProvider = SurveyTracert::find();

        //object of the Spreadsheet class to create the excel data
        $spreadsheet = new Spreadsheet();

        //add some data in excel cells
        $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A1', 'kdptimsmh')
        ->setCellValue('B1', 'kdpstmsmh')
        ->setCellValue('C1', 'nimhsmsmh')
        ->setCellValue('D1', 'nmmhsmsmh')
        ->setCellValue('E1', 'telpomsmh')
        ->setCellValue('F1', 'emailmsmh')
        ->setCellValue('G1', 'tahun_lulus')
        ->setCellValue('H1', 'f21')
        ->setCellValue('I1', 'f22')
        ->setCellValue('J1', 'f23')
        ->setCellValue('K1', 'f24')
        ->setCellValue('L1', 'f25')
        ->setCellValue('M1', 'f26')
        ->setCellValue('N1', 'f27')
        ->setCellValue('O1', 'f301')
        ->setCellValue('P1', 'f302 ')
        ->setCellValue('Q1', 'f303 ')
        ->setCellValue('R1', 'f401')
        ->setCellValue('S1', 'f402')
        ->setCellValue('T1', 'f403')
        ->setCellValue('U1', 'f404')
        ->setCellValue('V1', 'f405')
        ->setCellValue('W1', 'f406')
        ->setCellValue('X1', 'f407')
        ->setCellValue('Y1', 'f408')
        ->setCellValue('Z1', 'f409')
        ->setCellValue('AA1', 'f410')
        ->setCellValue('AB1', 'f411')
        ->setCellValue('AC1', 'f412')
        ->setCellValue('AD1', 'f413')
        ->setCellValue('AE1', 'f414')
        ->setCellValue('AF1', 'f415')
        ->setCellValue('AG1', 'f416')
        ->setCellValue('AH1', 'f6 ')
        ->setCellValue('AI1', 'f501')
        ->setCellValue('AJ1', 'f502 ')
        ->setCellValue('AK1', 'f503 ')
        ->setCellValue('AL1', 'f7 ')
        ->setCellValue('AM1', 'f7a')
        ->setCellValue('AN1', 'f8')
        ->setCellValue('AO1', 'f901')
        ->setCellValue('AP1', 'f902')
        ->setCellValue('AQ1', 'f903')
        ->setCellValue('AR1', 'f904')
        ->setCellValue('AS1', 'f905')
        ->setCellValue('AT1', 'f906')
        ->setCellValue('AU1', 'f1001')
        ->setCellValue('AV1', 'f1002')
        ->setCellValue('AW1', 'f1101')
        ->setCellValue('AX1', 'f1102')
        ->setCellValue('AY1', 'f1201')
        ->setCellValue('AZ1', 'f1202')
        ->setCellValue('BA1', 'f1301 ')
        ->setCellValue('BB1', 'f1302 ')
        ->setCellValue('BC1', 'f1303 ')
        ->setCellValue('BD1', 'f14')
        ->setCellValue('BE1', 'f15')
        ->setCellValue('BF1', 'f1601')
        ->setCellValue('BG1', 'f1602')
        ->setCellValue('BH1', 'f1603')
        ->setCellValue('BI1', 'f1604')
        ->setCellValue('BJ1', 'f1605')
        ->setCellValue('BK1', 'f1606')
        ->setCellValue('BL1', 'f1607')
        ->setCellValue('BM1', 'f1608')
        ->setCellValue('BN1', 'f1609')
        ->setCellValue('BO1', 'f1610')
        ->setCellValue('BP1', 'f1611')
        ->setCellValue('BQ1', 'f1612')
        ->setCellValue('BR1', 'f1613')
        ->setCellValue('BS1', 'f1614')
        ->setCellValue('BT1', 'f1701')
        ->setCellValue('BU1', 'f1702b')
        ->setCellValue('BV1', 'f1703')
        ->setCellValue('BW1', 'f1704b')
        ->setCellValue('BX1', 'f1705')
        ->setCellValue('BY1', 'f1705a')
        ->setCellValue('BZ1', 'f1706')
        ->setCellValue('CA1', 'f1706ba')
        ->setCellValue('CB1', 'f1707')
        ->setCellValue('CC1', 'f1708b')
        ->setCellValue('CD1', 'f1709')
        ->setCellValue('CE1', 'f1710b')
        ->setCellValue('CF1', 'f1711')
        ->setCellValue('CG1', 'f1711a')
        ->setCellValue('CH1', 'f1712b')
        ->setCellValue('CI1', 'f1712a')
        ->setCellValue('CJ1', 'f1713')
        ->setCellValue('CK1', 'f1714b')
        ->setCellValue('CL1', 'f1715')
        ->setCellValue('CM1', 'f1716b')
        ->setCellValue('CN1', 'f1717')
        ->setCellValue('CO1', 'f1718b')
        ->setCellValue('CP1', 'f1719')
        ->setCellValue('CQ1', 'f1720b')
        ->setCellValue('CR1', 'f1721')
        ->setCellValue('CS1', 'f1722b')
        ->setCellValue('CT1', 'f1723')
        ->setCellValue('CU1', 'f1724b')
        ->setCellValue('CV1', 'f1725')
        ->setCellValue('CW1', 'f1726b')
        ->setCellValue('CX1', 'f1727')
        ->setCellValue('CY1', 'f1728b')
        ->setCellValue('CZ1', 'f1729')
        ->setCellValue('DA1', 'f1730b')
        ->setCellValue('DB1', 'f1731')
        ->setCellValue('DC1', 'f1732b')
        ->setCellValue('DD1', 'f1733')
        ->setCellValue('DE1', 'f1734b')
        ->setCellValue('DF1', 'f1735')
        ->setCellValue('DG1', 'f1736b')
        ->setCellValue('DH1', 'f1737')
        ->setCellValue('DI1', 'f1737a')
        ->setCellValue('DJ1', 'f1738')
        ->setCellValue('DK1', 'f1738ba')
        ->setCellValue('DL1', 'f1739')
        ->setCellValue('DM1', 'f1740b')
        ->setCellValue('DN1', 'f1741')
        ->setCellValue('DO1', 'f1742b')
        ->setCellValue('DP1', 'f1743')
        ->setCellValue('DQ1', 'f1744b')
        ->setCellValue('DR1', 'f1745')
        ->setCellValue('DS1', 'f1746b')
        ->setCellValue('DT1', 'f1747')
        ->setCellValue('DU1', 'f1748b')
        ->setCellValue('DV1', 'f1749')
        ->setCellValue('DW1', 'f1750b')
        ->setCellValue('DX1', 'f1751')
        ->setCellValue('DY1', 'f1752b')
        ->setCellValue('DZ1', 'f1753')
        ->setCellValue('EA1', 'f1754b');

        $startPoint = 2;

        foreach($dataProvider->all() as $v){
            $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A'.$startPoint, $v->kdptimsmh)
            ->setCellValue('B'.$startPoint, $v->kdpstmsmh)
            ->setCellValue('C'.$startPoint, $v->nimhsmsmh)
            ->setCellValue('D'.$startPoint, $v->nmmhsmsmh)
            ->setCellValue('E'.$startPoint, $v->telpomsmh)
            ->setCellValue('F'.$startPoint, $v->emailmsmh)
            ->setCellValue('G'.$startPoint, $v->tahun_lulus)
            ->setCellValue('H'.$startPoint, $v->f21)
            ->setCellValue('I'.$startPoint, $v->f22)
            ->setCellValue('J'.$startPoint, $v->f23)
            ->setCellValue('K'.$startPoint, $v->f24)
            ->setCellValue('L'.$startPoint, $v->f25)
            ->setCellValue('M'.$startPoint, $v->f26)
            ->setCellValue('N'.$startPoint, $v->f27)
            ->setCellValue('O'.$startPoint, $v->f301)
            ->setCellValue('P'.$startPoint, $v->f302)
            ->setCellValue('Q'.$startPoint, $v->f303)
            ->setCellValue('R'.$startPoint, $v->f401)
            ->setCellValue('S'.$startPoint, $v->f402)
            ->setCellValue('T'.$startPoint, $v->f403)
            ->setCellValue('U'.$startPoint, $v->f404)
            ->setCellValue('V'.$startPoint, $v->f405)
            ->setCellValue('W'.$startPoint, $v->f406)
            ->setCellValue('X'.$startPoint, $v->f407)
            ->setCellValue('Y'.$startPoint, $v->f408)
            ->setCellValue('Z'.$startPoint, $v->f409)
            ->setCellValue('AA'.$startPoint, $v->f410)
            ->setCellValue('AB'.$startPoint, $v->f411)
            ->setCellValue('AC'.$startPoint, $v->f412)
            ->setCellValue('AD'.$startPoint, $v->f413)
            ->setCellValue('AE'.$startPoint, $v->f414)
            ->setCellValue('AF'.$startPoint, $v->f415)
            ->setCellValue('AG'.$startPoint, $v->f416)
            ->setCellValue('AH'.$startPoint, $v->f6)
            ->setCellValue('AI'.$startPoint, $v->f501)
            ->setCellValue('AJ'.$startPoint, $v->f502z)
            ->setCellValue('AK'.$startPoint, $v->f503)
            ->setCellValue('AL'.$startPoint, $v->f7)
            ->setCellValue('AM'.$startPoint, $v->f7a)
            ->setCellValue('AN'.$startPoint, $v->f8)
            ->setCellValue('AO'.$startPoint, $v->f901)
            ->setCellValue('AP'.$startPoint, $v->f902)
            ->setCellValue('AQ'.$startPoint, $v->f903)
            ->setCellValue('AR'.$startPoint, $v->f904)
            ->setCellValue('AS'.$startPoint, $v->f905)
            ->setCellValue('AT'.$startPoint, $v->f906)
            ->setCellValue('AU'.$startPoint, $v->f1001)
            ->setCellValue('AV'.$startPoint, $v->f1002)
            ->setCellValue('AW'.$startPoint, $v->f1101)
            ->setCellValue('AX'.$startPoint, $v->f1102)
            ->setCellValue('AY'.$startPoint, $v->f1201)
            ->setCellValue('AZ'.$startPoint, $v->f1202)
            ->setCellValue('BA'.$startPoint, $v->f1301)
            ->setCellValue('BB'.$startPoint, $v->f1302)
            ->setCellValue('BC'.$startPoint, $v->f1303)
            ->setCellValue('BD'.$startPoint, $v->f14)
            ->setCellValue('BE'.$startPoint, $v->f15)
            ->setCellValue('BF'.$startPoint, $v->f1601)
            ->setCellValue('BG'.$startPoint, $v->f1602)
            ->setCellValue('BH'.$startPoint, $v->f1603)
            ->setCellValue('BI'.$startPoint, $v->f1604)
            ->setCellValue('BJ'.$startPoint, $v->f1605)
            ->setCellValue('BK'.$startPoint, $v->f1606)
            ->setCellValue('BL'.$startPoint, $v->f1607)
            ->setCellValue('BM'.$startPoint, $v->f1608)
            ->setCellValue('BN'.$startPoint, $v->f1609)
            ->setCellValue('BO'.$startPoint, $v->f1610)
            ->setCellValue('BP'.$startPoint, $v->f1611)
            ->setCellValue('BQ'.$startPoint, $v->f1612)
            ->setCellValue('BR'.$startPoint, $v->f1613)
            ->setCellValue('BS'.$startPoint, $v->f1614)
            ->setCellValue('BT'.$startPoint, $v->f1701)
            ->setCellValue('BU'.$startPoint, $v->f1702b)
            ->setCellValue('BV'.$startPoint, $v->f1703)
            ->setCellValue('BW'.$startPoint, $v->f1704b)
            ->setCellValue('BX'.$startPoint, $v->f1705)
            ->setCellValue('BY'.$startPoint, $v->f1705a)
            ->setCellValue('BZ'.$startPoint, $v->f1706)
            ->setCellValue('CA'.$startPoint, $v->f1706ba)
            ->setCellValue('CB'.$startPoint, $v->f1707)
            ->setCellValue('CC'.$startPoint, $v->f1708b)
            ->setCellValue('CD'.$startPoint, $v->f1709)
            ->setCellValue('CE'.$startPoint, $v->f1710b)
            ->setCellValue('CF'.$startPoint, $v->f1711)
            ->setCellValue('CG'.$startPoint, $v->f1711a)
            ->setCellValue('CH'.$startPoint, $v->f1712b)
            ->setCellValue('CI'.$startPoint, $v->f1712a)
            ->setCellValue('CJ'.$startPoint, $v->f1713)
            ->setCellValue('CK'.$startPoint, $v->f1714b)
            ->setCellValue('CL'.$startPoint, $v->f1715)
            ->setCellValue('CM'.$startPoint, $v->f1716b)
            ->setCellValue('CN'.$startPoint, $v->f1717)
            ->setCellValue('CO'.$startPoint, $v->f1718b)
            ->setCellValue('CP'.$startPoint, $v->f1719)
            ->setCellValue('CQ'.$startPoint, $v->f1720b)
            ->setCellValue('CR'.$startPoint, $v->f1721)
            ->setCellValue('CS'.$startPoint, $v->f1722b)
            ->setCellValue('CT'.$startPoint, $v->f1723)
            ->setCellValue('CU'.$startPoint, $v->f1724b)
            ->setCellValue('CV'.$startPoint, $v->f1725)
            ->setCellValue('CW'.$startPoint, $v->f1726b)
            ->setCellValue('CX'.$startPoint, $v->f1727)
            ->setCellValue('CY'.$startPoint, $v->f1728b)
            ->setCellValue('CZ'.$startPoint, $v->f1729)
            ->setCellValue('DA'.$startPoint, $v->f1730b)
            ->setCellValue('DB'.$startPoint, $v->f1731)
            ->setCellValue('DC'.$startPoint, $v->f1732b)
            ->setCellValue('DD'.$startPoint, $v->f1733)
            ->setCellValue('DE'.$startPoint, $v->f1734b)
            ->setCellValue('DF'.$startPoint, $v->f1735)
            ->setCellValue('DG'.$startPoint, $v->f1736b)
            ->setCellValue('DH'.$startPoint, $v->f1737)
            ->setCellValue('DI'.$startPoint, $v->f1737a)
            ->setCellValue('DJ'.$startPoint, $v->f1738)
            ->setCellValue('DK'.$startPoint, $v->f1738ba)
            ->setCellValue('DL'.$startPoint, $v->f1739)
            ->setCellValue('DM'.$startPoint, $v->f1740b)
            ->setCellValue('DN'.$startPoint, $v->f1741)
            ->setCellValue('DO'.$startPoint, $v->f1742b)
            ->setCellValue('DP'.$startPoint, $v->f1743)
            ->setCellValue('DQ'.$startPoint, $v->f1744b)
            ->setCellValue('DR'.$startPoint, $v->f1745)
            ->setCellValue('DS'.$startPoint, $v->f1746b)
            ->setCellValue('DT'.$startPoint, $v->f1747)
            ->setCellValue('DU'.$startPoint, $v->f1748b)
            ->setCellValue('DV'.$startPoint, $v->f1749)
            ->setCellValue('DW'.$startPoint, $v->f1750b)
            ->setCellValue('DX'.$startPoint, $v->f1751)
            ->setCellValue('DY'.$startPoint, $v->f1752b)
            ->setCellValue('DZ'.$startPoint, $v->f1753)
            ->setCellValue('EA'.$startPoint, $v->f1754b);

            $startPoint++;
        }
        
        //set style for A1,B1,C1 cells
        $cell_st =[
         'font' =>['bold' => true],
         'alignment' =>['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER],
         'borders'=>['bottom' =>['style'=> \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM]]
        ];
        $spreadsheet->getActiveSheet()->getStyle('A1:EA1')->applyFromArray($cell_st);

        //set columns width
        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(16);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(18);

        $spreadsheet->getActiveSheet()->setTitle('Tracert'); //set a title for Worksheet

        //make object of the Xlsx class to save the excel file
        // $writer = new Xlsx($spreadsheet);
        $fxls ='tracert-study-res_'.time().'.xlsx';
        
        // $writer->save($fxls);
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$fxls.'"');
        // download file 
        $writer->save("php://output");
    }


    /**
     * Displays a single SurveyTracert model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SurveyTracert model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SurveyTracert();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->nimhsmsmh]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SurveyTracert model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->nimhsmsmh]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SurveyTracert model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SurveyTracert model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return SurveyTracert the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SurveyTracert::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
