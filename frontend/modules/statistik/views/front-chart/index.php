<?php
use yii\helpers\Url;
$this->title = "Statistik";
?>
<h1>Statistik</h1>
<div class="row">
	<div class="col-md-6">
	    <div id="chartByTotal"></div>
	</div>

	<div class="col-md-6">
	    <div id="chartBySex"></div>		
	</div>

	<div class="col-md-6">
	    <div id="chartByProdi"></div>		
	</div>

	<div class="col-md-6">
	    <div id="chartByTahunLulus"></div>		
	</div>
</div>

<?php 
$script =<<<JS
goLoad({elm:'div#chartByTotal', url:'/statistik/front-chart/by-total'});
goLoad({elm:'div#chartBySex', url:'/statistik/front-chart/by-sex'});
goLoad({elm:'div#chartByProdi', url:'/statistik/front-chart/by-prodi'});
goLoad({elm:'div#chartByTahunLulus', url:'/statistik/front-chart/by-tahun-lulus'});
JS;
$this->registerJsFile(Url::base(true).'/vendor/bower-asset/chart.js/dist/Chart.min.js');
$this->registerCssFile(Url::base(true).'/vendor/bower-asset/chart.js/dist/Chart.min.css');
$this->registerJs($script);