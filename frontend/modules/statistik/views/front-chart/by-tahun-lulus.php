<?php 
$arrval = [];
foreach($m_stat_profile->all() as $v){
    $arrval[] = ['nilai' => $v->nmmhsmsmh, 'tahun' => $v->tahun_lulus];
}

$label_tahun= "'".implode("','", array_column($arrval, 'tahun'))."'";
$label_nilai = implode(',', array_column($arrval, 'nilai'));
$counting = count($arrval);
?>
<h3>Berdasarkan Tahun Lulus</h3>
<canvas id="my_total_tahun_lulus"></canvas>
<?php 
$script =<<<JS
var kuler = ['#6dd3ce', '#c8e9a0', '#f7a278', '#351e29', '#f7567c', '#a13d63', '#2c9c69', '#dbba34','#ffdd4a', '#49516f', '#8ea4d2', '#6279b8', '#fcfcfc', '#fffae3', '#6dd3ce', '#dff2d8', '#c6dea6', '#a13d63', '#7a6263'];
var ctx = document.getElementById('my_total_tahun_lulus').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'bar',

    // The data for our dataset
    data: {
        labels: [{$label_tahun}],
        datasets: [{
            label: 'Berdasarkan Tahun Lulus',
            data: [{$label_nilai}],
            backgroundColor: kuler.splice(0, {$counting})
        }]
    }
});
JS;
$this->registerJs($script);