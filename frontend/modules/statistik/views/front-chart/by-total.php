<h3>Berdasarkan pengguna/mahasiswa</h3>
<canvas id="my_total_chart"></canvas>
<?php 
$c_user = $m_stat_user->count();
$c_profile = $m_stat_profile->count();
$c_survey = $m_stat_survey->count();
$script =<<<JS
var ctx = document.getElementById('my_total_chart').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'bar',
    // The data for our dataset
    data: {
        labels: ['Jml Mahasiswa(M)', 'Jml M isi Profile', 'Jml M isi Survey'],
        datasets: [{
            label: 'Berdasarkan Jumlah Pengguna',
            data: [{$c_user}, {$c_profile}, {$c_survey}],
            backgroundColor: ['#2c9c69', '#dbba34', '#c62f29']
        }]
    }
});
JS;
$this->registerJs($script);