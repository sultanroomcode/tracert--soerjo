<?php 
$arrval = [
    '21201' => ['nilai' => 0, 'nama' => 'Teknik Mesin'],
    '22201' => ['nilai' => 0, 'nama' => 'Teknik Sipil'],
    '54211' => ['nilai' => 0, 'nama' => 'Agroteknologi'],
    '61201' => ['nilai' => 0, 'nama' => 'Manajemen'],
    '63201' => ['nilai' => 0, 'nama' => 'Ilmu Administrasi Negara'],
    '74201' => ['nilai' => 0, 'nama' => 'Ilmu Hukum']
];//init val
foreach($m_stat_profile->all() as $v){
    $arrval[$v->kdpstmsmh]['nilai'] = $v->nmmhsmsmh;
}

// var_dump($arrval);
?>
<h3>Berdasarkan Program Studi</h3>
<canvas id="my_total_prodi"></canvas>
<?php 
$label_prodi = "'".implode("','", array_column($arrval, 'nama'))."'";
$label_nilai = implode(',', array_column($arrval, 'nilai'));
$script =<<<JS
var ctx = document.getElementById('my_total_prodi').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'bar',

    // The data for our dataset
    data: {
        labels: [{$label_prodi}],
        datasets: [{
            label: 'Berdasarkan Program Studi',
            data: [{$label_nilai}],
            backgroundColor: ['#2c9c69', '#dbba34','#ffdd4a', '#49516f', '#8ea4d2', '#6279b8']
        }]
    },

    // Configuration options go here
    options: {}
});
JS;
$this->registerJs($script);