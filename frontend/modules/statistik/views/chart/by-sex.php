<?php
$arr = [0,0];
$i = 0;
foreach($m_stat_profile->all() as $v){
    $arr[$i] = $v->nmmhsmsmh;
    $i++;
}
?>
<h3>Berdasarkan jenis kelamin</h3>
<canvas id="my_total_gender"></canvas>
<?php
$txt_val = implode(',', $arr); 
$script =<<<JS
var ctx = document.getElementById('my_total_gender').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'pie',

    // The data for our dataset
    data: {
        labels: ['Laki-Laki', 'Perempuan'],
        datasets: [{
            data: [{$txt_val}],
            backgroundColor: ['#2c9c69', '#dbba34']
        }]
    },

    // Configuration options go here
    options: {}
});
JS;
$this->registerJs($script);