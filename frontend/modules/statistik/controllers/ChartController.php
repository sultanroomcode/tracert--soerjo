<?php
namespace frontend\modules\statistik\controllers;

use yii\web\Controller;
use common\models\User;
use frontend\modules\survey\models\SurveyTracert;
use frontend\modules\profile\models\UserProfile;
use yii\filters\AccessControl;

class ChartController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'by-sex', 'by-total', 'by-prodi', 'by-tahun-lulus'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionByTotal()
    {
        $user = User::find()->where(['type' => 'mahasiswa']);
        $profile = UserProfile::find();
        $survey = SurveyTracert::find();

        return $this->renderAjax('by-total', [
            'm_stat_user' => $user,
            'm_stat_profile' => $profile,
            'm_stat_survey' => $survey,
        ]);
    }

    public function actionBySex()
    {
        $profile = UserProfile::find()->select(['COUNT(*) AS nmmhsmsmh', 'gender'])->groupBy(['gender']);

        return $this->renderAjax('by-sex', [
            'm_stat_profile' => $profile,
        ]);
    }

    public function actionByProdi()
    {
        $profile = UserProfile::find()->select(['COUNT(*) AS nmmhsmsmh', 'kdpstmsmh'])->groupBy(['kdpstmsmh'])->orderBy('kdpstmsmh');

        return $this->renderAjax('by-prodi', [
            'm_stat_profile' => $profile,
        ]);
    }

    public function actionByTahunLulus()
    {
        $profile = UserProfile::find()->select(['COUNT(*) AS nmmhsmsmh', 'tahun_lulus'])->groupBy(['tahun_lulus']);

        return $this->renderAjax('by-tahun-lulus', [
            'm_stat_profile' => $profile,
        ]);
    }
}
