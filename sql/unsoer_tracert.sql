-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 09, 2019 at 05:07 PM
-- Server version: 5.7.26-0ubuntu0.18.04.1
-- PHP Version: 7.2.18-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `unsoer_tracert`
--

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1560042051),
('m190608_024936_survey', 1560042094);

-- --------------------------------------------------------

--
-- Table structure for table `survey_tracert`
--

CREATE TABLE `survey_tracert` (
  `nimhsmsmh` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `f501` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `f502` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `f503` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `f1201` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `f1202` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `f8` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `f14` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `f15` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `f1301` decimal(12,2) DEFAULT NULL,
  `f1302` decimal(12,2) DEFAULT NULL,
  `f1303` decimal(12,2) DEFAULT NULL,
  `f21` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f22` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f23` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f24` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f25` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f26` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f27` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f301` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f302` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f303` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f401` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f402` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f403` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f404` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f405` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f406` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f407` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f408` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f409` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f410` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f411` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f412` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f413` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f414` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f415` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f6` smallint(6) DEFAULT NULL,
  `f7` smallint(6) DEFAULT NULL,
  `f7a` smallint(6) DEFAULT NULL,
  `f901` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f902` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f903` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f904` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f905` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f906` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1001` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1002` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1101` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1102` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1601` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1602` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1603` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1604` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1605` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1606` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1607` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1608` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1609` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1610` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1611` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1612` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1613` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1614` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1701` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1702b` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1703` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1704b` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1705` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1706b` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1705a` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1706ba` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1707` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1708b` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1709` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1710b` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1711` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1712b` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1713` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1714b` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1715` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1716b` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1717` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1718b` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1719` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1720b` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1721` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1722b` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1723` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1724b` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1725` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1726b` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1727` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1728b` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1729` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1730b` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1731` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1732b` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1733` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1734b` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1735` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1736b` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1737` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1738b` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1737a` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1738ba` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1739` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1740b` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1741` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1742b` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1743` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1744b` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1745` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1746b` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1747` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1748b` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1749` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1750b` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1751` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1752b` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1753` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1754b` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_profile`
--

CREATE TABLE `user_profile` (
  `nimhsmsmh` varchar(15) COLLATE utf8_unicode_ci NOT NULL COMMENT 'NIM',
  `kdptimsmh` varchar(15) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Kode PT',
  `kdpstmsmh` varchar(15) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Kode Prodi',
  `nmmhsmsmh` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Nama Mahasiswa',
  `tahun_lulus` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `telpomsmh` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `emailmsmh` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
