<?php

/* @var $this yii\web\View */

$this->title = 'Administrasi Tracert Studi Universitas Soerjo';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Selamat Datang Admin!</h1>

        <p class="lead">anda telah mengakses aplikasi administrasi tracert studi</p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>User</h2>

                <p>Untuk administrasi pengguna sistem dan mahasiswa anda bisa mengaturnya disini</p>
            </div>
            <div class="col-lg-4">
                <h2>Profile</h2>

                <p>Untuk isian profile yang diisikan mahasiswa anda bisa mengaturnya di modul tersebut.</p>
            </div>
            <div class="col-lg-4">
                <h2>Survey</h2>

                <p>Anda bisa mengakses survey yang telah diisikan oleh mahasiswa.</p>
            </div>
        </div>

    </div>
</div>
