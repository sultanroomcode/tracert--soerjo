<?php
use yii\db\Migration;

class m190608_024936_survey extends Migration
{
    public function safeUp()
    {
    	$tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_profile}}', [
            'nimhsmsmh' => $this->string(15)->notNull(),//NIM
            'kdptimsmh' => $this->string(15)->notNull(),//Kode PT/071045
            'kdpstmsmh' => $this->string(15)->notNull(),//Kode Prodi
            'nmmhsmsmh' => $this->string()->notNull(),//Nama Mhs
            'tahun_lulus' => $this->string(4)->notNull(),
            'telpomsmh' => $this->string(40)->notNull(),
            'emailmsmh' => $this->string(150)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addCommentOnColumn('user_profile','nimhsmsmh','NIM');
        $this->addCommentOnColumn('user_profile','kdptimsmh','Kode PT');
        $this->addCommentOnColumn('user_profile','kdpstmsmh','Kode Prodi');
        $this->addCommentOnColumn('user_profile','nmmhsmsmh','Nama Mahasiswa');

        $this->createTable('{{%survey_tracert}}', [
            'nimhsmsmh' => $this->string(15)->notNull(),//NIM
            'f501' => $this->string(1)->notNull(),//range 1|2
            'f502' => $this->string(3)->notNull(),//contoh isian xxx bulan
            'f503' => $this->string(3)->notNull(),

            'f1201' => $this->string(1)->notNull(),//range 1-7
            'f1202' => $this->string(200)->notNull(),//isian

            'f8' => $this->string(1)->notNull(),//range 1|2
            'f14' => $this->string(1)->notNull(),//range 1-5
            'f15' => $this->string(1)->notNull(),//range 1-4
            'f1301' => $this->money(12,2),
            'f1302' => $this->money(12,2),
            'f1303' => $this->money(12,2),

            //optional
            'f21' => $this->string(1),//range 1-5
            'f22' => $this->string(1),//range 1-5
            'f23' => $this->string(1),//range 1-5
            'f24' => $this->string(1),//range 1-5
            'f25' => $this->string(1),//range 1-5
            'f26' => $this->string(1),//range 1-5
            'f27' => $this->string(1),//range 1-5
            
            'f301' => $this->string(1),//range 1-3
            'f302' => $this->string(2),//uraian bulan
            'f303' => $this->string(2),//uraian bulan
            
            'f401' => $this->string(1),
            'f402' => $this->string(1),
            'f403' => $this->string(1),
            'f404' => $this->string(1),
            'f405' => $this->string(1),
            'f406' => $this->string(1),
            'f407' => $this->string(1),
            'f408' => $this->string(1),
            'f409' => $this->string(1),
            'f410' => $this->string(1),
            'f411' => $this->string(1),
            'f412' => $this->string(1),
            'f413' => $this->string(1),
            'f414' => $this->string(1),
            'f415' => $this->string(1),
            
            'f6' => $this->smallInteger(),//uraian jumlah
            'f7' => $this->smallInteger(),//uraian jumlah
            'f7a' => $this->smallInteger(),//uraian jumlah

            'f901' => $this->string(1),
            'f902' => $this->string(1),
            'f903' => $this->string(1),
            'f904' => $this->string(1),
            'f905' => $this->string(1),
            'f906' => $this->string(),//Uraian

            'f1001' => $this->string(1),
            'f1002' => $this->string(),//Uraian

            'f1101' => $this->string(1),
            'f1102' => $this->string(),//Uraian
            
            'f1601' => $this->string(2),
            'f1602' => $this->string(2),
            'f1603' => $this->string(2),
            'f1604' => $this->string(2),
            'f1605' => $this->string(2),
            'f1606' => $this->string(2),
            'f1607' => $this->string(2),
            'f1608' => $this->string(2),
            'f1609' => $this->string(2),
            'f1610' => $this->string(2),
            'f1611' => $this->string(2),
            'f1612' => $this->string(2),
            'f1613' => $this->string(2),
            'f1614' => $this->string(200),
            
            'f1701' => $this->string(1),//A - Pengetahuan di bidang atau disiplin ilmu anda f17-1 f17-2b
            'f1702b' => $this->string(1),//B
            'f1703' => $this->string(1),//A - Pengetahuan di luar bidang atau disiplin ilmu anda f17-3 f17-4b
            'f1704b' => $this->string(1),//B
            'f1705' => $this->string(1),//A - Pengetahuan umum f17-5 f17-6b
            'f1706b' => $this->string(1),//B
            'f1705a' => $this->string(1),//A - Bahasa Inggris f17-5a f17-6ba
            'f1706ba' => $this->string(1),//B
            'f1707' => $this->string(1),//A - Ketrampilan internet f17-7 f17-8b 
            'f1708b' => $this->string(1),//B
            'f1709' => $this->string(1),//A - Ketrampilan komputer f17-9 f17-10b
            'f1710b' => $this->string(1),//B
            'f1711' => $this->string(1),//A - Berpikir kritis f17-11 f17-12b
            'f1712b' => $this->string(1),//B
            'f1713' => $this->string(1),//A - Ketrampilan riset f17-13 f17-14b 
            'f1714b' => $this->string(1),//B
            'f1715' => $this->string(1),//A - Kemampuan belajar f17-15 f17-16b
            'f1716b' => $this->string(1),//B
            'f1717' => $this->string(1),//A - Kemampuan berkomunikasi f17-17 f17-18b
            'f1718b' => $this->string(1),//B
            'f1719' => $this->string(1),//A - Bekerja di bawah tekanan f17-19 f17-20b
            'f1720b' => $this->string(1),//B
            'f1721' => $this->string(1),//A - Manajemen waktu f17-21 f17-22b
            'f1722b' => $this->string(1),//B
            'f1723' => $this->string(1),//A - Bekerja secara mandiri f17-23 f17-24b
            'f1724b' => $this->string(1),//B
            'f1725' => $this->string(1),//A - Bekerja dalam tim/bekerjasama dengan orang lain f17-25 f17-26b
            'f1726b' => $this->string(1),//B
            'f1727' => $this->string(1),//A - Kemampuan dalam memecahkan masalah f17-27 f17-28b
            'f1728b' => $this->string(1),//B
            'f1729' => $this->string(1),//A - Negosiasi f17-29 f17-30b
            'f1730b' => $this->string(1),//B
            'f1731' => $this->string(1),//A - Kemampuan analisis f17-31 f17-32b
            'f1732b' => $this->string(1),//B
            'f1733' => $this->string(1),//A - Toleransi f17-33 f17-34b
            'f1734b' => $this->string(1),//B
            'f1735' => $this->string(1),//A  - Kemampuan adaptasi f17-35 f17-36b
            'f1736b' => $this->string(1),//B
            'f1737' => $this->string(1),//A - Loyalitasf17-37 f17-38b
            'f1738b' => $this->string(1),//B
			'f1737a' => $this->string(1),//A - Integritas f17-37A f17-38ba
            'f1738ba' => $this->string(1),//B
            'f1739' => $this->string(1),//A - Bekerja dengan orang yang berbeda budaya maupun latar belakang f17-39 f17-40b
            'f1740b' => $this->string(1),//B
            'f1741' => $this->string(1),//A - Kepemimpinan f17-41 f17-42b
            'f1742b' => $this->string(1),//B
            'f1743' => $this->string(1),//A - Kemampuan dalam memegang tanggungjawab f17-43 f17-44b
            'f1744b' => $this->string(1),//B
            'f1745' => $this->string(1),//A - Inisiatif f17-45 f17-46b
            'f1746b' => $this->string(1),//B
            'f1747' => $this->string(1),//A - Manajemen proyek/program f17-47 f17-48b
            'f1748b' => $this->string(1),//B
            'f1749' => $this->string(1),//A - Kemampuan untuk memresentasikan ide/produk/laporan f17-49 f17-50b
            'f1750b' => $this->string(1),//B
            'f1751' => $this->string(1),//A - Kemampuan dalam menulis laporan, memo dan dokumen f17-51 f17-52b
            'f1752b' => $this->string(1),//B
            'f1753' => $this->string(1),//A - Kemampuan untuk terus belajar sepanjang hayat f17-53 f17-54b
            'f1754b' => $this->string(1),//B


            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%user_profile}}');
        $this->dropTable('{{%survey}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190608_024936_survey cannot be reverted.\n";

        return false;
    }
    */
}
