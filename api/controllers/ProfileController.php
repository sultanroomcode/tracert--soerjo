<?php
namespace api\controllers;

use yii\rest\ActiveController;
use Yii;
use api\models\Event;
use api\models\User;
use api\models\UserProfile;
use yii\filters\auth\QueryParamAuth;

class ProfileController extends ActiveController
{
    public $modelClass = 'api\models\UserProfile';

    public function actions()
    {
    	$actions = parent::actions();
    	unset($actions['index']);
    	unset($actions['create']);
    	unset($actions['update']);
    	unset($actions['view']);
    	unset($actions['delete']);
    	return $actions;
    }

    public function behaviors()
	{
	    $behaviors = parent::behaviors();
	    $behaviors['authenticator'] = [
	        'class' => QueryParamAuth::className(),
	    ];
	    return $behaviors;
	}

	public function actionView()
    {
	    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
	    $attributes = Yii::$app->request->get();

	    $profile = UserProfile::findOne(['nimhsmsmh' => $attributes['nim'] ]);
	    if($profile != null)
	    {
	    	return ['status' => 1, 'data' => $profile, 'message' => 'update'];
	    }
	    else
	    {
	    	return ['status'=> 0, 'data' => null, 'message' => 'create'];
	    }
    }

    public function actionCreate()
	{
	    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

	    $us = User::findOne(['username' => Yii::$app->request->post('nimhsmsmh')]);
	    if($us == null)
	    {
	    	return ['status' => 0, 'data' => null, 'message' => 'tidak ada user terkait'];
	    }

	    $us = UserProfile::findOne(['nimhsmsmh' => Yii::$app->request->post('nimhsmsmh')]);
	    if($us != null)
	    {
	    	$profile->attributes = Yii::$app->request->post();
	    	if($profile->validate())
	    	{
				$profile->save();
		    	return ['status' => 1, 'data' => $profile, 'message' => 'data sudah ada! dan telah di update'];
	    	} else {
	    		return ['status' => 0, 'data' => $profile->getErrors(), 'message' => 'Masih ada attribute yang belum terisi'];
	    	}
	    }

		$profile = new UserProfile();
		$profile->scenario = UserProfile:: SCENARIO_CREATE;
		
		$data = Yii::$app->request->post();
		$v = explode('-', $data['kdpstmsmh']);
		$data['kdpstmsmh'] = $v[0];

		/*if($data['gender'] == 'Laki-Laki')
		{
			$data['gender'] = 'L';
		} else {
			$data['gender'] = 'P';
		}*/

		$profile->attributes = $data;

		if($profile->validate())
		{
			$profile->save();
			return ['status' => 1, 'data' => $profile, 'message' => 'Data telah tersimpan'];
		}
		else
		{
			return ['status' => 0, 'data' => $profile->getErrors(), 'message' => 'Masih ada attribute yang belum terisi'];
		}
	}

    public function actionUpdate()
	{
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;     
		$attributes = Yii::$app->request->post();

		$profile = UserProfile::findOne(['nimhsmsmh' => $attributes['nimhsmsmh'] ]);
	    if($profile != null)
	    {
	    	$profile->attributes = Yii::$app->request->post();
	    	if($profile->validate())
	    	{
				$profile->save();
				//update user email too
				$model = User::findOne(['username' => $attributes['nimhsmsmh']]);
				$model->email = $attributes['emailmsmh'];
				$model->save(false);

		    	return ['status' => 1, 'data'=> $profile, 'message' => 'Data telah diupdate'];
	    	} else {
	    		return ['status' => 0, 'data' => $profile->getErrors(), 'message' => 'Masih ada attribute yang belum terisi'];
	    	}
	    }
	    else
	    {
	    	//new
	    	$n_user = new UserProfile();
	    	$n_user->attributes = Yii::$app->request->post();
			if($n_user->validate())
			{
				$n_user->save();
				return ['status' => 1, 'data'=> $n_user, 'message' => 'User Profile telah disimpan'];
			}
			else
			{
				return ['status' => 0, 'data' => $n_user->getErrors(), 'message' => 'Masih ada attribute yang belum terisi'];
			}
	    }
	}
}
