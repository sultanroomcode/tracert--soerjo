<?php
namespace api\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\QueryParamAuth;
use api\models\LoginForm;
use api\models\User;

class SiteController extends Controller
{
	public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'authenticatior' => [
                'class'=> CompositeAuth::className(), // Implementing access token authentication
                'authMethods' => [
                    QueryParamAuth::className(),
                    // \sizeg\jwt\JwtHttpBearerAuth::class
                ],
                'except'=> ['login', 'pass'], /// There is no need to validate the access token method. Note the distinction between $noAclLogin
            ]
        ]);
    }

    public function beforeAction($action) 
    { 
        $this->enableCsrfValidation = false; 
        return parent::beforeAction($action); 
    }

    public function actionTest()
    {
        return ['status'=>'success'];
    }

    public function actionIndex()
    {
    	$data = 'Welcome to API, please login first, to access data';
    	return $data;
    }

    public function actionPass()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $data = User::findOne(1);
        $data->setPassword('enteraja');
        $data->save();
        return $data;
    }

    public function actionLogin()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;  
        $model = new LoginForm();
        /*ob_flush();
        ob_start();
        var_dump(Yii::$app->request);
        file_put_contents("dump.txt", ob_get_flush());*/

        if ($model->load(Yii::$app->request->post(), '') && $model->login()) {
            return ($model->login());
        } else {
            return ($model->getFirstErrors());
        }
    }
}
