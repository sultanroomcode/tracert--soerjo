<?php
namespace api\controllers;

use yii\rest\ActiveController;
use Yii;
use api\models\Event;
use yii\filters\auth\QueryParamAuth;

class EventController extends ActiveController
{
    public $modelClass = 'api\models\Event';

    public function actions()
    {
    	$actions = parent::actions();
    	unset($actions['index']);
    	unset($actions['create']);
    	unset($actions['update']);
    	unset($actions['view']);
    	unset($actions['delete']);
    	return $actions;
    }

    public function behaviors()
	{
	    $behaviors = parent::behaviors();
	    $behaviors['authenticator'] = [
	        'class' => QueryParamAuth::className(),
	    ];
	    return $behaviors;
	}

    public function actionCreate()
	{
	    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$event = new Event();
		$event->scenario = Event:: SCENARIO_CREATE;
		$event->attributes = Yii::$app->request->post();

		if($event->validate())
		{
			$event->save();
			return ['status' => true, 'data' => 'event sudah dibuat'];
		}
		else
		{
			return ['status' => false, 'data' => $event->getErrors()];
		}
	}

	public function actionIndex()
    {
	    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
	    $events = Event::find()->all();
	    if(count($events) > 0)
	    {
	    	return ['status' => true, 'data'=> $events];
	    }
	    else
	    {
	    	return ['status'=>false, 'data'=> 'Belum ada event'];
	    }
    }

    public function actionView()
    {
	    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
	    $attributes = Yii::$app->request->get();

	    $events = Event::findOne(['id' => $attributes['id'] ]);
	    if($events != null)
	    {
	    	return ['status' => true, 'data'=> $events];
	    }
	    else
	    {
	    	return ['status'=>false, 'data'=> 'Event tidak ditemukan'];
	    }
    }

    public function actionUpdate()
	{
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;     
		$attributes = Yii::$app->request->get();

		$event = Event::findOne(['id' => $attributes['id'] ]);
	    if($event != null)
	    {
	    	$event->attributes = Yii::$app->request->post();
			$event->save();
	    	return ['status' => true, 'data'=> $event];
	    }
	    else
	    {
	    	return ['status'=>false, 'data'=> 'Event tidak ditemukan'];
	    }
	}

	public function actionDelete()
	{
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;     
		$attributes = Yii::$app->request->get();

		$event = $event2 = Event::findOne(['id' => $attributes['id'] ]);
	    if($event != null)
	    {
	    	$event->attributes = Yii::$app->request->post();
			$event->delete();
	    	return ['status' => true, 'data'=> 'event '.$event2->title.' dihapus' ];
	    }
	    else
	    {
	    	return ['status'=>false, 'data'=> 'Event tidak ditemukan'];
	    }
	}
}
