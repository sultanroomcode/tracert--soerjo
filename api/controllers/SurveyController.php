<?php
namespace api\controllers;

use yii\rest\ActiveController;
use Yii;
use api\models\SurveyTracert;
use api\models\User;
use api\models\UserProfile;
use yii\filters\auth\QueryParamAuth;

class SurveyController extends ActiveController
{
    public $modelClass = 'api\models\SurveyTracert';
    private $npm = null;

    public function actions()
    {
    	$actions = parent::actions();
    	unset($actions['index']);
    	unset($actions['create']);
    	unset($actions['update']);
    	unset($actions['view']);
    	unset($actions['delete']);
    	return $actions;
    }

    /*public function behaviors()
	{
	    $behaviors = parent::behaviors();
	    $behaviors['authenticator'] = [
	        'class' => QueryParamAuth::className(),
	    ];
	    return $behaviors;
	}*/

    public function filter_query()
    {
    	$us_a = User::findOne(['username' => $this->npm]);
	    if($us_a == null)
	    {
	    	return ['status' => 0, 'data' => null, 'message' => 'tidak ada user terkait'];
	    }

		$us_b = UserProfile::findOne(['nimhsmsmh' => $this->npm]);
	    if($us_b == null)
	    {
	    	return ['status' => 0, 'data' => null, 'message' => 'silahkan mengisi profile dulu'];
	    }
    }

    public function actionCreate()
	{
	    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		
		$this->npm = Yii::$app->request->post('nimhsmsmh');
		$this->filter_query();

	    $us_b = SurveyTracert::findOne(['nimhsmsmh' => $this->npm]);
	    if($us_b != null)
	    {
	    	return ['status' => 0, 'data' => null, 'message' => 'data sudah ada silahkan untuk mengupdate data'];
	    }

	    $survey = new SurveyTracert();
		$survey->scenario = SurveyTracert:: SCENARIO_CREATE;
		$survey->attributes = Yii::$app->request->post();

		if($survey->validate())
		{
			$survey->save();
			return ['status' => 1, 'data' => 'survey sudah dibuat'];
			if($survey->save())
			{
		    	return ['status' => 1, 'data'=> $survey, 'message' => 'Berhasil update data !'];
			} else {
				return ['status' => 0, 'data'=> $survey->getErrors(), 'message' => 'Beberapa Form Belum Diisi !'];
			}
		}
		else
		{
			return ['status' => 0, 'data' => $survey->getErrors()];
		}
	}

    public function actionView()
    {
	    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
	    $attributes = Yii::$app->request->get();

	    $survey = SurveyTracert::findOne(['nimhsmsmh' => $attributes['id'] ]);
	    if($survey != null)
	    {
	    	return ['status' => 1, 'data'=> $survey];
	    }
	    else
	    {
	    	return ['status'=> 0, 'data'=> 'Survey tidak ditemukan'];
	    }
    }

    public function actionUpdate()
	{
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;     
		$attributes = Yii::$app->request->get();
		$this->npm = Yii::$app->request->post('nimhsmsmh');
		$this->filter_query();

		$survey = SurveyTracert::findOne(['nimhsmsmh' => $this->npm]);
	    if($survey != null)
	    {
	    	$survey->attributes = Yii::$app->request->post();
			if($survey->save())
			{
		    	return ['status' => 1, 'data'=> $survey, 'message' => 'Berhasil update data !'];
			} else {
				return ['status' => 0, 'data'=> $survey->getErrors(), 'message' => 'Beberapa Form Belum Diisi !'];
			}
	    }
	    else
	    {
	    	return ['status'=> 0, 'data'=> 'Survey tidak ditemukan'];
	    }
	}

	public function actionDelete()
	{
		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;     
		$attributes = Yii::$app->request->get();

		$survey = $survey2 = SurveyTracert::findOne(['id' => $attributes['id'] ]);
	    if($survey != null)
	    {
	    	$survey->attributes = Yii::$app->request->post();
			$survey->delete();
	    	return ['status' => 1, 'data'=> 'survey '.$survey2->title.' dihapus' ];
	    }
	    else
	    {
	    	return ['status'=> 0, 'data'=> 'Survey tidak ditemukan'];
	    }
	}
}
