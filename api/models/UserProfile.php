<?php
namespace api\models;

use Yii;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "user_profile".
 *
 * @property string $nimhsmsmh NIM
 * @property string $kdptimsmh Kode PT
 * @property string $kdpstmsmh Kode Prodi
 * @property string $nmmhsmsmh Nama Mahasiswa
 * @property string $tahun_lulus
 * @property string $telpomsmh
 * @property string $emailmsmh
 * @property int $created_at
 * @property int $updated_at
 */
class UserProfile extends ActiveRecord
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    public static function tableName()
    {
        return 'user_profile';
    }

    public function behaviors()
    {
        return ['timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 // 'value' => new \yii\db\Expression('NOW()'),
             ],];
    }
    
    public function rules()
    {
        return [
            [['nimhsmsmh', 'kdptimsmh', 'kdpstmsmh', 'nmmhsmsmh', 'tahun_lulus', 'telpomsmh', 'emailmsmh'], 'required'],
            [['nimhsmsmh', 'kdptimsmh', 'kdpstmsmh'], 'string', 'max' => 15],
            [['nmmhsmsmh'], 'string', 'max' => 255],
            [['tahun_lulus'], 'string', 'max' => 4],
            [['gender'], 'string', 'max' => 1],
            [['telpomsmh'], 'string', 'max' => 40],
            [['emailmsmh'], 'string', 'max' => 150],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['nimhsmsmh', 'nmmhsmsmh', 'gender','kdpstmsmh','kdptimsmh', 'tahun_lulus', 'telpomsmh', 'emailmsmh']; 
        $scenarios['update'] = ['nimhsmsmh', 'nmmhsmsmh', 'gender','kdpstmsmh', 'tahun_lulus', 'telpomsmh']; 
        return $scenarios; 
    }
    
    public function attributeLabels()
    {
        return [
            'nimhsmsmh' => 'NIM',
            'kdptimsmh' => 'Kode PT',
            'kdpstmsmh' => 'Kode Prodi',
            'nmmhsmsmh' => 'Nama Mahasiswa',
            'tahun_lulus' => 'Tahun Lulus',
            'telpomsmh' => 'Telpon Mahasiswa',
            'emailmsmh' => 'Email Mahasiswa',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function parserGender()//untuk meng-aliaskan
    {
        $arr = [
            0 => 'L', 
            1 => 'P',
        ];

        return $arr; 
    }

    public function parserProdi()//untuk meng-aliaskan
    {
        $arr = [
            0 => '54211',//Agroteknologi 
            1 => '63201',//Ilmu Administrasi Negara 
            2 => '74201',//Ilmu Hukum 
            3 => '61201',//Manajemen 
            4 => '21201',//Teknik Mesin 
            5 => '22201',//Teknik Sipil
        ];

        return $arr; 
    }

    public function parserProdiText()//untuk meng-aliaskan
    {
        $arr = [
            '54211' => 'Agroteknologi',
            '63201' => 'Ilmu Administrasi Negara',
            '74201' => 'Ilmu Hukum',
            '61201' => 'Manajemen',
            '21201' => 'Teknik Mesin',
            '22201' => 'Teknik Sipil',
        ];

        return $arr; 
    }

    public function fields()//untuk meng-aliaskan
    {
        return [
            'nim' => 'nimhsmsmh',
            'kd_prodi' => 'kdpstmsmh',
            'nama' => 'nmmhsmsmh',
            'no_telpon' => 'telpomsmh',
            'email' => 'emailmsmh',
            'tahun_lulus' => 'tahun_lulus',
            'tanggal_buat' => function () {
                return date(DATE_RFC3339, $this->created_at);
            },
            'tanggal_update' => function () {
                return date(DATE_RFC3339, $this->created_at);
            },
        ];
    }

    public function getUseria()
    {
        return $this->hasOne(User::className(), ['username' => 'nimhsmsmh']);
    }

    public function getSurvey()
    {
        return $this->hasOne(SurveyTracert::className(), ['nimhsmsmh' => 'nimhsmsmh']);
    }
}
