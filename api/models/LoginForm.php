<?php
namespace api\models;

use Yii;
use yii\base\Model;
use api\models\User;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    const EXPIRE_TIME = 604800;
    private $_user;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            // return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
            if ($this->getUser()) {
                // if expired token
                if($this->_user->expired_at < time())
                {
                    //then refresh
                    $access_token = $this->_user->generateAccessToken();
                    $this->_user->expired_at = time() + static::EXPIRE_TIME;
                    $this->_user->save();
                    Yii::$app->user->login($this->_user, static::EXPIRE_TIME);
                    $arr = [
                        'username' => $this->_user->username, 
                        'email' => $this->_user->email, 
                        'access_token' => $access_token, 
                        'expire' => $this->_user->expired_at, 
                        'status' => '1', 
                        'message' => 'refresh',
                        'profile_status' => ($this->_user->profil != null)?'1':'0',//profile 1 update, 0 create
                        'survey_status' => ($this->_user->survey != null)?'1':'0'//profile 1 update, 0 create
                    ];
                } else {
                    $arr = [
                        'username' => $this->_user->username, 
                        'email' => $this->_user->email, 
                        'access_token' => $this->_user->access_token, 
                        'expire' => $this->_user->expired_at, 
                        'status' => '1', 
                        'message' => 'still-active',
                        'profile_status' => ($this->_user->profil != null)?'1':'0',//profile 1 update, 0 create
                        'survey_status' => ($this->_user->survey != null)?'1':'0'//profile 1 update, 0 create
                    ];
                }

                if($this->_user->profil != null)
                {
                    $profile = $this->_user->profil;
                    $arr['profile_data'] = [
                        'kd_prodi' => $profile->kdpstmsmh,
                        'nama' => $profile->nmmhsmsmh,
                        'tahun_lulus' => $profile->tahun_lulus,
                        'telpon' => $profile->telpomsmh,
                        'gender' => $profile->gender,
                    ];
                } else {
                    $arr['profile_data'] = null;
                }

                return $arr;
            }
        }
        
        return ['access_token' => null, 'expire' => null, 'status' => '0', 'message' => $this->getErrors()];
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
}
