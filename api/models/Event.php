<?php
namespace api\models;

use Yii;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "event".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class Event extends ActiveRecord
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';
    public $status_txt;
    public static function tableName()
    {
        return 'event';
    }

    public function behaviors()
    {
        return ['timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
                 // 'value' => new \yii\db\Expression('NOW()'),
             ],];
    }

    public function rules()
    {
        return [
            [['title', 'status'], 'required'],
            [['description'], 'string'],
            [['status'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['title','description','status']; 
        return $scenarios; 
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function fields()//untuk meng-aliaskan
    {
        return [
            'id' => 'id',
            'judul' => 'title',
            'konten' => 'description',
            'status' => 'status',
            'tanggal_buat' => function () {
                return date(DATE_RFC3339, $this->created_at);
            },
            'tanggal_update' => function () {
                return date(DATE_RFC3339, $this->created_at);
            },
        ];
    }
}
